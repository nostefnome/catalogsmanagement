﻿using CatalogsManagementTool.MessageTracker.Contract.Enums;
using CatalogsManagementTool.MessageTracker.Contract.Models;
using Newtonsoft.Json.Linq;

namespace CatalogsManagementTool.MessageTracker.Web.Utils;

public static class ButtonActionUtils
{
    public static string BuildJsonButtonAction<T>(ActionType type, T request) where T : notnull
    {
        var action = new ButtonActionModel
        {
            Type = type,
            Request = JObject.FromObject(request)
        };
        return JObject.FromObject(action).ToString();
    }
}
