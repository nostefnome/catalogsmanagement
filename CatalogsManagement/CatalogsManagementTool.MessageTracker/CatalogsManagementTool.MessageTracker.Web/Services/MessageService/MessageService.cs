﻿using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.MessageTracker.Web.RequestConverter;
using MediatR;
using VkNet.Model;

namespace CatalogsManagementTool.MessageTracker.Web.Services.MessageService;

public class MessageServices : IMessageService
{
    private readonly IMediator _mediator;
    private readonly IRequestConverter _requestConverter;

    public MessageServices(IMediator mediator, IRequestConverter requestConverter)
    {
        _mediator = mediator;
        _requestConverter = requestConverter;
    }

    [Log]
    public async Task ProcessMessage(Message message)
    {
        var request = _requestConverter.Convert(message);

        if (request is null)
        {
            return;
        }

        await _mediator.Send(request);
    }
}
