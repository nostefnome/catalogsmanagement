﻿using VkNet.Model;

namespace CatalogsManagementTool.MessageTracker.Web.Services.MessageService;

public interface IMessageService
{
    Task ProcessMessage(Message message);
}
