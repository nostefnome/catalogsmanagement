﻿using VkNet.Model.RequestParams;

namespace CatalogsManagementTool.MessageTracker.Web.Services.ResponseService;

public interface IResponseService
{
    Task Send(MessagesSendParams response);
}
