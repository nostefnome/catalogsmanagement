﻿using CatalogsManagementTool.Core.Logging;
using VkNet.Abstractions;
using VkNet.Model.RequestParams;

namespace CatalogsManagementTool.MessageTracker.Web.Services.ResponseService;

public class ResponseService : IResponseService
{
    private readonly IVkApi _vkApi;

    public ResponseService (IVkApi vkApi)
    {
        _vkApi = vkApi;
    }

    [Log]
    public async Task Send(MessagesSendParams response)
    {
        await _vkApi.Messages.SendAsync(response);
    }
}
