﻿using CatalogsManagementTool.MessageTracker.Contract.Requests;
using CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuBuilderHandlers;
using CatalogsManagementTool.MessageTracker.Web.Services.ResponseService;
using MediatR;
using VkNet.Model.RequestParams;

namespace CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuHandlers;

public class ShowMainMenuHandler : IRequestHandler<ShowMainMenuRequest, Unit>
{
    private readonly IResponseService _responseService;
    private readonly IKeyboardBuilder<BuildMainKeyboardRequest> _keyboardBuilder;

    public ShowMainMenuHandler(
        IResponseService responseService,
        IKeyboardBuilder<BuildMainKeyboardRequest> keyboardBuilder)
    {
        _responseService = responseService;
        _keyboardBuilder = keyboardBuilder;
    }

    public async Task<Unit> Handle(ShowMainMenuRequest request, CancellationToken cancellationToken)
    {
        var board = _keyboardBuilder.Build(new BuildMainKeyboardRequest
        {
            PeerId = request.PeerId,
        });
        var message = new MessagesSendParams
        {
            RandomId = DateTime.Now.Ticks,
            PeerId = request.PeerId,
            Message = "Main menu",
            Keyboard = board
        };

        await _responseService.Send(message);

        return Unit.Value;
    }
}