﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Contract.Requests;
using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.MessageTracker.Contract.Enums;
using CatalogsManagementTool.MessageTracker.Contract.Requests;
using CatalogsManagementTool.MessageTracker.Web.Services.ResponseService;
using CatalogsManagementTool.MessageTracker.Web.Utils;
using MediatR;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.Keyboard;
using VkNet.Model.RequestParams;

namespace CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuHandlers;

public class ShowTitlesHandler : IRequestHandler<ShowTitlesRequest, Unit>
{
    private readonly IResponseService _responseService;
    private readonly ICatalogClient _catalogClient;

    public ShowTitlesHandler(IResponseService responseService, ICatalogClient catalogClient)
    {
        _responseService = responseService;
        _catalogClient = catalogClient;
    }

    [Log]
    public async Task<Unit> Handle(ShowTitlesRequest request, CancellationToken cancellationToken)
    {
        var titles = (await _catalogClient.GetTitles(request.CatalogName))
            .ToList();
        var message = new MessagesSendParams
        {
            RandomId = DateTime.Now.Ticks,
            PeerId = request.PeerId,
            Message = BuildMessage(titles, request),
            Keyboard = BuildMessageKeyboard(request.PeerId)
        };

        await _responseService.Send(message);

        return Unit.Value;
    }

    private MessageKeyboard BuildMessageKeyboard(long peerId)
    {
        var builder = new KeyboardBuilder();

        builder.AddButton(new MessageKeyboardButtonAction
        {
            Type = KeyboardButtonActionType.Text,
            Label = "Catalogs",
            Payload = ButtonActionUtils
                .BuildJsonButtonAction(ActionType.ShowCatalogs, new ShowCatalogsRequest
                {
                    PeerId = peerId
                })
        });

        return builder.Build();
    }

    private string BuildMessage(IEnumerable<TitleModel> titles, ShowTitlesRequest request)
    {
        if (titles.Any())
        {
            var titleList = titles
                .Select(title => $"{title.Name} - {title.Tag}({title.Count})")
                .Aggregate((a, b) => $"{a}\n\r{b}");

            return $"Catalog {request.CatalogName}:\n\r{titleList}";
        }

        return $"Catalog {request.CatalogName} empty";
    }
}