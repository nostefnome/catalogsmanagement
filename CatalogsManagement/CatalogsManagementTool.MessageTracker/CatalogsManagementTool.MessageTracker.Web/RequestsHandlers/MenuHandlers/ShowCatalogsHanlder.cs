﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.MessageTracker.Contract.Requests;
using CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuBuilderHandlers;
using CatalogsManagementTool.MessageTracker.Web.Services.ResponseService;
using MediatR;
using VkNet.Model.RequestParams;

namespace CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuHandlers;

public class ShowCatalogsHanlder : IRequestHandler<ShowCatalogsRequest, Unit>
{
    private readonly ICatalogClient _catalogClient;
    private readonly IResponseService _responseService;
    private readonly IKeyboardBuilder<BuildCatalogsKeyboardRequests> _keyboardBuilder;

    public ShowCatalogsHanlder(
        ICatalogClient catalogClient,
        IResponseService responseService,
        IKeyboardBuilder<BuildCatalogsKeyboardRequests> keyboardBuilder)
    {
        _catalogClient = catalogClient;
        _responseService = responseService;
        _keyboardBuilder = keyboardBuilder;
    }

    [Log]
    public async Task<Unit> Handle(ShowCatalogsRequest request, CancellationToken cancellationToken)
    {
        var catalogs = (await _catalogClient.GetAllCatalogs())
            .ToList();
        var keyboard = _keyboardBuilder.Build(new BuildCatalogsKeyboardRequests
        {
            PeerId = request.PeerId,
            Catalogs = catalogs
        });
        var message = new MessagesSendParams
        {
            RandomId = DateTime.Now.Ticks,
            PeerId = request.PeerId,
            Message = $"All catalogs:\n\r{BuildCatalogsString(catalogs)}",
            Keyboard = keyboard
        };

        await _responseService.Send(message);

        return Unit.Value;
    }

    private string BuildCatalogsString(IEnumerable<CatalogModel> catalogs)
    {
        return catalogs
            .Select(catalog => $"  {catalog.Name}")
            .Aggregate((a, b) => $"{a}\n\r{b}");
    }
}