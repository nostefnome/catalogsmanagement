﻿using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.MessageTracker.Contract.Enums;
using CatalogsManagementTool.MessageTracker.Contract.Requests;
using CatalogsManagementTool.MessageTracker.Web.Utils;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.Keyboard;

namespace CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuBuilderHandlers;

public class BuildCatalogsKeyboardRequests
{
    public IEnumerable<CatalogModel> Catalogs { get; set; }
    public long PeerId { get; set; }
}

public class CatalogsKeyboardBuilderHandler : IKeyboardBuilder<BuildCatalogsKeyboardRequests>
{
    [Log]
    public MessageKeyboard Build(BuildCatalogsKeyboardRequests request)
    {
        var builder = new KeyboardBuilder();

        foreach (var catalog in request.Catalogs)
        {
            builder.AddButton(new MessageKeyboardButtonAction
            {
                Type = KeyboardButtonActionType.Text,
                Label = catalog.Name,
                Payload = ButtonActionUtils
                    .BuildJsonButtonAction(ActionType.ShowTitles, new ShowTitlesRequest
                    {
                        CatalogName = catalog.Name,
                        PeerId = request.PeerId
                    })
            });
        }

        builder.AddButton(new MessageKeyboardButtonAction
        {
            Type = KeyboardButtonActionType.Text,
            Label = "Main menu",
            Payload = ButtonActionUtils
                .BuildJsonButtonAction(ActionType.MainMenu, new ShowMainMenuRequest
                {
                    PeerId = request.PeerId
                })
        });

        return builder.Build();
    }
}
