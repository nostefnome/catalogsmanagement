﻿using VkNet.Model.Keyboard;

namespace CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuBuilderHandlers;

public interface IKeyboardBuilder<TRequest> 
    where TRequest : class
{
    MessageKeyboard Build(TRequest request);
}
