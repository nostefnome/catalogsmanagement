﻿using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.MessageTracker.Contract.Enums;
using CatalogsManagementTool.MessageTracker.Contract.Requests;
using CatalogsManagementTool.MessageTracker.Web.Utils;
using VkNet.Enums.SafetyEnums;
using VkNet.Model.Keyboard;

namespace CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuBuilderHandlers;

public class BuildMainKeyboardRequest
{
    public long PeerId { get; set; }
}

public class MainKeyboardBuilderHandler : IKeyboardBuilder<BuildMainKeyboardRequest>
{
    [Log]
    public MessageKeyboard Build(BuildMainKeyboardRequest request)
    {
        var builder = new KeyboardBuilder();

        builder.AddButton(new MessageKeyboardButtonAction
        {
            Type = KeyboardButtonActionType.Text,
            Label = "Catalogs",
            Payload = ButtonActionUtils
                .BuildJsonButtonAction(ActionType.ShowCatalogs, new ShowCatalogsRequest
                {
                    PeerId = request.PeerId
                })
        });

        return builder.Build();
    }
}