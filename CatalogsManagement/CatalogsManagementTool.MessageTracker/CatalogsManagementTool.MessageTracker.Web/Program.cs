using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Core.Models.Vk;
using CatalogsManagementTool.Core.Settings;
using CatalogsManagementTool.Core.Settings.LoggingSettings;
using CatalogsManagementTool.MessageTracker.Web.RequestConverter;
using CatalogsManagementTool.MessageTracker.Web.RequestsHandlers.MenuBuilderHandlers;
using CatalogsManagementTool.MessageTracker.Web.Services.MessageService;
using CatalogsManagementTool.MessageTracker.Web.Services.ResponseService;
using CorrelationId;
using CorrelationId.DependencyInjection;
using CorrelationId.HttpClient;
using MediatR;
using Microsoft.OpenApi.Models;
using RestEase.HttpClientFactory;
using Serilog;
using Swashbuckle.AspNetCore.Filters;
using System.Text.Json.Serialization;
using VkNet;
using VkNet.Abstractions;
using VkNet.Model;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration((context, config) =>
    GlobalConfigurationSettings.Configure(context, config));
builder.Host.UseSerilog((context, loggerConfiguration) =>
    SerilogLoggingSettings.ConfigureSerilogLogger(context, loggerConfiguration));

AddConfig(builder.Services, builder.Configuration);

var app = builder.Build();
var env = app.Environment;

if (env.EnvironmentName == "IsolatedDevelopment" || env.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseCorrelationId();
app.UseHttpLogging();

app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint(
    "/swagger/v1/swagger.json", "CatalogsManagementTool.MessageTracker v1"));

app.UseRouting();

app.UseSerilogRequestLogging(options =>
{
    SerilogLoggingSettings.ConfigureSerilogRequestLogging(options);
});

//app.UseAuthentication();
//app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();

void AddConfig(IServiceCollection services, IConfiguration config)
{
    services
        .AddControllers()
        .AddNewtonsoftJson()
        .AddJsonOptions(options =>
        {
            options.JsonSerializerOptions.Converters.Add(new JsonStringEnumMemberConverter());
        });

    services.AddDefaultCorrelationId(options =>
        CorrelationIdSettings.Configure(options));
    services.AddHttpLogging(options =>
        HttpLoggingSettings.Configure(options));

    services.AddScoped<IVkApi>(services =>
    {
        var vkApi = new VkApi();
        vkApi.Authorize(new ApiAuthParams
        {
            AccessToken = config["AccessToken"]
        });

        return vkApi;
    });
    services.AddScoped(services => new VkConfirmation
    {
        ConfirmationValue = config["Confirmation"]
    });

    services.AddScoped<IMessageService, MessageServices>();
    services.AddScoped<IResponseService, ResponseService>();
    services.AddScoped<IRequestConverter, RequestConverter>();

    services.AddScoped<IKeyboardBuilder<BuildMainKeyboardRequest>, MainKeyboardBuilderHandler>();
    services.AddScoped<IKeyboardBuilder<BuildCatalogsKeyboardRequests>, CatalogsKeyboardBuilderHandler>();

    services.AddMediatR(typeof(Program));

    services.AddRestEaseClient<ICatalogClient>(config["Urls:Catalog"])
        .AddCorrelationIdForwarding();

    services.AddScoped(services => new RequestConverterSettings
    {
        DialogStartCommand = config["dialogStartCommand"]
    });

    services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "CatalogsManagementTool.MessageTracker",
            Version = "v1"
        });
        c.ExampleFilters();
        c.CustomSchemaIds(type => type.ToString());
        c.EnableAnnotations();
    });
    services.AddSwaggerExamples();
}