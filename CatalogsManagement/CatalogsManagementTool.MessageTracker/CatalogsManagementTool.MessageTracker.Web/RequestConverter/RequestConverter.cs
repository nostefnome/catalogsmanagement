﻿using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.MessageTracker.Contract.Enums;
using CatalogsManagementTool.MessageTracker.Contract.Models;
using CatalogsManagementTool.MessageTracker.Contract.Requests;
using Newtonsoft.Json.Linq;
using VkNet.Model;

namespace CatalogsManagementTool.MessageTracker.Web.RequestConverter;

public class RequestConverterSettings 
{
    public string DialogStartCommand { get; set; }
}

public class RequestConverter : IRequestConverter
{
    private readonly Dictionary<ActionType, Type> _requestsTypes = new()
    {
        [ActionType.MainMenu] = typeof(ShowMainMenuRequest),
        [ActionType.ShowCatalogs] = typeof(ShowCatalogsRequest),
        [ActionType.ShowTitles] = typeof(ShowTitlesRequest)
    };
    private readonly RequestConverterSettings _converterSettings;

    public RequestConverter(RequestConverterSettings converterSettings)
    {
        _converterSettings = converterSettings;
    }

    [Log]
    public object Convert(Message message)
    {
        if (message.Payload is not null)
        {
            var action = JObject
                .Parse(message.Payload)
                .ToObject<ButtonActionModel>();
            var type = _requestsTypes[action.Type];

            return action.Request.ToObject(type);
        }

        return isStartDialog(message) ?
            new ShowMainMenuRequest { PeerId = (long)message.PeerId } :
            null;
    }

    private bool isStartDialog(Message message)
    {
        return string.Equals(message.Text, _converterSettings.DialogStartCommand, 
            StringComparison.CurrentCultureIgnoreCase);
    }
}
