﻿using VkNet.Model;

namespace CatalogsManagementTool.MessageTracker.Web.RequestConverter;

public interface IRequestConverter
{
    object Convert(Message message);
}