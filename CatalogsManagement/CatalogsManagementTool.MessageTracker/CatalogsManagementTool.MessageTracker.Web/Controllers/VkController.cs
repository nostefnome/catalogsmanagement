﻿using CatalogsManagementTool.Core.Models.Vk;
using CatalogsManagementTool.MessageTracker.Web.Services.MessageService;
using CatalogsManagementTool.VkCircuitBreaker;
using Microsoft.AspNetCore.Mvc;
using VkNet.Model;

namespace CatalogsManagementTool.MessageTracker.Web.Controllers;

[Route("api/vk/message")]
[ApiController]
public class VkController : Controller
{
    private readonly IMessageService _messageService;
    private readonly VkConfirmation _confirmation;

    public VkController(IMessageService messageService, VkConfirmation confirmation)
    {
        _messageService = messageService;
        _confirmation = confirmation;
    }

    [HttpPost("[action]")]
    [EnsureRequestDoesNotContainRetry]
    public async Task<IActionResult> Callback([FromBody] VkEvent vkEvent)
    {
        if (vkEvent.Type is VkEventType.Confirmation)
        {
            return Ok(_confirmation.ConfirmationValue);
        }

        if (vkEvent.Type is not VkEventType.NewMessage)
        {
            return BadRequest($"Type {vkEvent.Type} does not supports.");
        }

        var message = Message.FromJson(new(vkEvent.Object));
        await _messageService.ProcessMessage(message);

        return Ok();
    }
}
