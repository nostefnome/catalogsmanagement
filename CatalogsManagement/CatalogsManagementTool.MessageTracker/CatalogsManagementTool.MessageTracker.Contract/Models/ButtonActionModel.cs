﻿using CatalogsManagementTool.MessageTracker.Contract.Enums;
using MediatR;
using Newtonsoft.Json.Linq;

namespace CatalogsManagementTool.MessageTracker.Contract.Models;

public class ButtonActionModel : IRequest
{
    public ActionType Type { get; set; }
    public JObject Request { get; set; }
}
