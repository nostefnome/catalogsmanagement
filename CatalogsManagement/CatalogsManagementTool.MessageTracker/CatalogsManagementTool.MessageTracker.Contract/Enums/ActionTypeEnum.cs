﻿namespace CatalogsManagementTool.MessageTracker.Contract.Enums;

public enum ActionType
{
    MainMenu,
    ShowCatalogs,
    ShowTitles
}