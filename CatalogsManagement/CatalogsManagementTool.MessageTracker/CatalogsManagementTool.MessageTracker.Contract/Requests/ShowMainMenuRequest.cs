﻿using MediatR;

namespace CatalogsManagementTool.MessageTracker.Contract.Requests;

public class ShowMainMenuRequest : IRequest
{
    public long PeerId { get; set; }
}
