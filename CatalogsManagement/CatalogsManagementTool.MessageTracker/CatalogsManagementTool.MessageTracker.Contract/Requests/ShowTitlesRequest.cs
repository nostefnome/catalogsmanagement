﻿using MediatR;

namespace CatalogsManagementTool.MessageTracker.Contract.Requests;

public class ShowTitlesRequest : IRequest
{
    public string CatalogName { get; set; }
    public long PeerId { get; set; }
}
