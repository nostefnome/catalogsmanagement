﻿using MediatR;

namespace CatalogsManagementTool.MessageTracker.Contract.Requests;

public class ShowCatalogsRequest : IRequest
{
    public long PeerId { get; set; }
}
