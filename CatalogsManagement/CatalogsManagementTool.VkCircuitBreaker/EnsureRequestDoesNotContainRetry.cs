﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace CatalogsManagementTool.VkCircuitBreaker
{
    public class EnsureRequestDoesNotContainRetry : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var retryHeaderContains = context.HttpContext.Request.Headers.Keys.Contains("X-Retry-Counter");

            if (retryHeaderContains)
            {
                context.Result = new ConflictResult();
            }
        }

        public override void OnActionExecuted(ActionExecutedContext context) { }
    }
}