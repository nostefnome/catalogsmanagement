﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CatalogsManagementTool.Catalog.Data.Entities;

public class Title
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid TitleId { get; set; }
    public Guid CatalogId { get; set; }

    public string Name { get; set; }
    public string Tag { get; set; }
    public int Count { get; set; }

    public virtual Catalog Catalog { get; set; }
}
