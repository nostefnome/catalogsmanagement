﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CatalogsManagementTool.Catalog.Data.Entities;

public class Catalog
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid CatalogId { get; set; }
    public string Name { get; set; }

    public virtual ICollection<Title> Titles { get; set; }
}