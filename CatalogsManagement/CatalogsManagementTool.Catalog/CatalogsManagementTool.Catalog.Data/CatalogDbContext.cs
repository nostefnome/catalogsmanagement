﻿using CatalogsManagementTool.Catalog.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Data;

public class CatalogDbContext : DbContext
{
    public DbSet<Entities.Catalog> Catalogs { get; set; }
    public DbSet<Title> Titles { get; set; }

    public CatalogDbContext(DbContextOptions<CatalogDbContext> dbContext) : base(dbContext)
    {

    }
}
