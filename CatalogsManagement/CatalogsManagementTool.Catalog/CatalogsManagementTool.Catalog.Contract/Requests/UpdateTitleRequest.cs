﻿using CatalogsManagementTool.Catalog.Data;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Contract.Requests;

public class UpdateTitleRequest
{
    public Guid TitleId { get; set; }
    public Guid CatalogId { get; set; }

    public string Name { get; set; }
    public string Tag { get; set; }
    public int Count { get; set; }
}

public class UpdateTitleRequestValidator : AbstractValidator<UpdateTitleRequest>
{
    private readonly CatalogDbContext _context;

    public UpdateTitleRequestValidator(CatalogDbContext context)
    {
        _context = context;

        RuleFor(title => title.CatalogId)
            .NotEmpty()
            .MustAsync(async (id, token) => await CatalogExists(id))
                .WithMessage(title => $"Catalog with id {title.CatalogId} does not exist");
        RuleFor(title => title.Count)
            .GreaterThan(0)
                .WithMessage(title => $"Title count must be more than zero");
        RuleFor(title => title.TitleId)
            .MustAsync(async (id, token) => await TitleExists(id))
                .WithMessage(title => $"Title with id {title.TitleId} does not exist");
    }

    private async Task<bool> CatalogExists(Guid id)
    {
        return await _context.Catalogs
            .AnyAsync(catalog => catalog.CatalogId == id);
    }

    private async Task<bool> TitleExists(Guid id)
    {
        return await _context.Titles
            .AnyAsync(title => title.TitleId == id);
    }
}