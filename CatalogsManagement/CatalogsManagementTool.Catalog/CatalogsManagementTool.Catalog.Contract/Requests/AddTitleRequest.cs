﻿using CatalogsManagementTool.Catalog.Data;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Contract.Requests;

public class AddTitleRequest
{
    public Guid CatalogId { get; set; }

    public string Name { get; set; }
    public string Tag { get; set; }
    public int Count { get; set; }
}

public class AddTitleRequestValidator : AbstractValidator<AddTitleRequest>
{
    private readonly CatalogDbContext _context;

    public AddTitleRequestValidator(CatalogDbContext context)
    {
        _context = context;

        RuleFor(title => title.CatalogId)
            .NotEmpty()
            .MustAsync(async (id, token) => await CatalogExists(id))
                .WithMessage(title => $"Catalog with id {title.CatalogId} does not exists");
        RuleFor(title => title.Name)
            .NotEmpty()
            .MustAsync(async (name, token) => await IsTitleNameUnique(name))
                .WithMessage(title => $"Title with name {title.Name} already exists");
        RuleFor(title => title.Count)
            .GreaterThan(0)
                .WithMessage(title => $"Title count must be more than zero");
    }

    private async Task<bool> IsTitleNameUnique(string name)
    {
        return await _context.Titles
            .AllAsync(title => title.Name != name);
    }

    private async Task<bool> CatalogExists(Guid id)
    {
        return await _context.Catalogs
            .AnyAsync(catalog => catalog.CatalogId == id);
    }
}