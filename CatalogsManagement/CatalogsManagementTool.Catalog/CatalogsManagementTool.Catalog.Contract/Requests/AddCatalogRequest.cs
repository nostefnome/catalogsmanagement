﻿using CatalogsManagementTool.Catalog.Data;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Contract.Requests;

public class AddCatalogRequest
{
    public string Name { get; set; }
}

public class AddCatalogRequestValidator : AbstractValidator<AddCatalogRequest>
{
    private readonly CatalogDbContext _catalogContext;

    public AddCatalogRequestValidator(CatalogDbContext catalogContext)
    {
        _catalogContext = catalogContext;

        RuleFor(request => request.Name)
            .NotEmpty()
            .MustAsync(async (name, token) => await IsCatalogUnique(name))
                .WithMessage(request => $"Catalog with name {request.Name} already exists");
    }

    private async Task<bool> IsCatalogUnique(string name)
    {
        return await _catalogContext.Catalogs
            .AllAsync(catalog => catalog.Name != name);
    }
}