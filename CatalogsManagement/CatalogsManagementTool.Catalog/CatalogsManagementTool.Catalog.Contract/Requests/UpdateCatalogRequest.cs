﻿using CatalogsManagementTool.Catalog.Data;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Contract.Requests;

public class UpdateCatalogRequest
{
    public Guid CatalogId { get; set; }
    public string Name { get; set; }
}

public class UpdateCatalogRequestValidator : AbstractValidator<UpdateCatalogRequest>
{
    private readonly CatalogDbContext _context;

    public UpdateCatalogRequestValidator(CatalogDbContext context)
    {
        _context = context;

        RuleFor(title => title.CatalogId)
            .NotEmpty()
            .MustAsync(async (id, token) => await CatalogExists(id))
                .WithMessage(title => $"Catalog with id {title.CatalogId} does not exist");
       
    }

    private async Task<bool> CatalogExists(Guid id)
    {
        return await _context.Catalogs
            .AnyAsync(catalog => catalog.CatalogId == id);
    }
}