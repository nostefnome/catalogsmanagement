﻿namespace CatalogsManagementTool.Catalog.Contract.Models;

public class CatalogModel
{
    public Guid CatalogId { get; set; }
    public string Name { get; set; }
}
