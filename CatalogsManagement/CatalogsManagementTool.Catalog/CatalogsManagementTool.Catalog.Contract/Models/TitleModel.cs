﻿namespace CatalogsManagementTool.Catalog.Contract.Models;

public class TitleModel
{
    public Guid TitleId { get; set; }
    public Guid CatalogId { get; set; }

    public string Name { get; set; }
    public string Tag { get; set; }
    public int Count { get; set; }
}
