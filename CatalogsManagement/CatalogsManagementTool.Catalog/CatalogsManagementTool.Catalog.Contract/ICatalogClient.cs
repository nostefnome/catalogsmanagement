﻿using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Contract.Requests;
using RestEase;

namespace CatalogsManagementTool.Catalog.Contract;

public interface ICatalogClient
{
    [Get("api/catalog/get/all")]
    Task<IEnumerable<CatalogModel>> GetAllCatalogs();

    [Get("api/catalog/get/{name}")]
    Task<CatalogModel> GetCatalogByName([Path] string name);

    [Post("api/catalog/add")]
    Task<Guid> AddCatalog([Body] AddCatalogRequest request);

    [Put("api/catalog/update")]
    Task UpdateCatalog([Body] UpdateCatalogRequest request);

    [Delete("api/catalog/remove/{catalogId}")]
    Task RemoveCatalog([Path] Guid catalogId);

    [Get("api/title/get")]
    Task<IEnumerable<TitleModel>> GetTitles([Query] string catalogName = null);

    [Get("api/title/get/{name}")]
    Task<TitleModel> GetTitleByName([Path] string name);

    [Post("api/title/add")]
    Task<Guid> AddTitle([Body] AddTitleRequest request);

    [Put("api/title/update")]
    Task UpdateTitle([Body] UpdateTitleRequest request);

    [Delete("api/title/remove/{titleId}")]
    Task RemoveTitle([Path] Guid titleId);
}
