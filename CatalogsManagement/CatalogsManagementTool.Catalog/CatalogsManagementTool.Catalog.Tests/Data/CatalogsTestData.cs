﻿using CatalogsManagementTool.Catalog.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CatalogsManagementTool.Catalog.Tests.Data
{
    internal static class CatalogsTestData
    {
        public static IEnumerable<Title> Titles = new[]
        {
            new Title
            {
                CatalogId = Guid.NewGuid(),
                Name = "Case1",
                Tag = "#tag1",
                Count = int.MaxValue,
            },
            new Title
            {
                CatalogId = Guid.NewGuid(),
                Name = "Case2",
                Tag = "#tag2",
                Count = int.MaxValue,
            },
            new Title
            {
                CatalogId = Guid.NewGuid(),
                Name = "Case3",
                Tag = "#tag3",
                Count = int.MaxValue,
            },
            new Title
            {
                CatalogId = Guid.NewGuid(),
                Name = "Case4",
                Tag = "#tag4",
                Count = int.MaxValue,
            },
        };

        public static IEnumerable<Catalog.Data.Entities.Catalog> Catalogs = new[]
        {
            new Catalog.Data.Entities.Catalog
            {
                CatalogId = Guid.NewGuid(),
                Name = "Case1",
                Titles = Titles.Take(2).ToList()
            },
            new Catalog.Data.Entities.Catalog
            {
                CatalogId = Guid.NewGuid(),
                Name = "Case2",
                Titles = Titles.Skip(2).Take(2).ToList()
            },
        };
    }
}
