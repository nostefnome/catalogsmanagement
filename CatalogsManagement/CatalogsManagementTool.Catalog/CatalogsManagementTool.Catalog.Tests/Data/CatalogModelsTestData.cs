﻿using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Tests.Utilities;
using System.Collections.Generic;
using System.Linq;

namespace CatalogsManagementTool.Catalog.Tests.Data;

internal static class CatalogModelsTestData
{
    public static IEnumerable<CatalogModel> Catalogs = CatalogsTestData.Catalogs
        .Select(catalog => MapperUtility.Mapper.Map<CatalogModel>(catalog));
    
    public static IEnumerable<TitleModel> Titles = CatalogsTestData.Titles
        .Select(title => MapperUtility.Mapper.Map<TitleModel>(title));
}
