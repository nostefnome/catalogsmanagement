﻿using CatalogsManagementTool.Catalog.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace CatalogsManagementTool.Catalog.Tests.Utilities;

internal static class DbContextUtility
{
    public static CatalogDbContext CreateCatalogDbContext(string name,
        IEnumerable<Catalog.Data.Entities.Catalog> catalogs = null,
        IEnumerable<Catalog.Data.Entities.Title> titles = null)
    {
        var db = new CatalogDbContext(
            new DbContextOptionsBuilder<CatalogDbContext>()
            .UseInMemoryDatabase(name)
            .Options);

        db.Database.EnsureDeleted();

        if (catalogs != null)
        {
            db.Catalogs.AddRange(catalogs);
        }

        if (titles != null)
        {
            db.Titles.AddRange(titles);
        }

        db.SaveChanges();

        return db;
    }
}
