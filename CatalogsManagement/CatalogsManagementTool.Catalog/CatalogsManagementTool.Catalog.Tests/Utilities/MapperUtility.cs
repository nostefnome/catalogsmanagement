﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Web;

namespace CatalogsManagementTool.Catalog.Tests.Utilities;

internal static class MapperUtility
{
    public static IMapper Mapper = new Mapper(
        new MapperConfiguration(cfg => cfg.AddProfile(new AutoMapperProfile())));
}
