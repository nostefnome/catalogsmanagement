﻿using CatalogsManagementTool.Catalog.Tests.Data;
using CatalogsManagementTool.Catalog.Web.Services.CatalogService;
using CatalogsManagementTool.Catalog.Tests.Utilities;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using System.Linq;
using System;
using FluentAssertions;

namespace CatalogsManagementTool.Catalog.Tests.Services;

public class CatalogServiceTests
{
    [Fact]
    public async Task GetAllCatalogsTest()
    {
        var dbContext = DbContextUtility.CreateCatalogDbContext(
            nameof(GetAllCatalogsTest),
            catalogs: CatalogsTestData.Catalogs);
        var service = new CatalogService(dbContext, MapperUtility.Mapper);

        var expected = CatalogModelsTestData.Catalogs;
        var actual = await service.GetAll();

        actual.Should().BeEquivalentTo(expected);
    }

    [Theory]
    [MemberData(nameof(CatalogServiceTestsMemberData.GetCatalogByIdTestsData), MemberType = typeof(CatalogServiceTestsMemberData))]
    public async Task GetCatalogByIdTest(Guid id)
    {
        var dbContext = DbContextUtility.CreateCatalogDbContext(
            nameof(GetCatalogByIdTest),
            catalogs: CatalogsTestData.Catalogs);
        var service = new CatalogService(dbContext, MapperUtility.Mapper);

        var actual = await service.GetById(id);

        Assert.Equal(id, actual.CatalogId);
    }

    [Fact]
    public async Task TryGetDoesNotExistProjectTest()
    {
        var dbContext = DbContextUtility.CreateCatalogDbContext(
            nameof(TryGetDoesNotExistProjectTest),
            catalogs: CatalogsTestData.Catalogs);
        var service = new CatalogService(dbContext, MapperUtility.Mapper);

        var actual = await service.GetById(Guid.NewGuid());

        Assert.Null(actual);
    }
}

public class CatalogServiceTestsMemberData
{
    public static IEnumerable<object[]> GetCatalogByIdTestsData => 
        CatalogModelsTestData.Catalogs
            .Select(catalog => new object[] { catalog.CatalogId });
}
