﻿using CatalogsManagementTool.Catalog.Tests.Data;
using CatalogsManagementTool.Catalog.Tests.Utilities;
using CatalogsManagementTool.Catalog.Web.Services.TitleService;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CatalogsManagementTool.Catalog.Tests.Services
{
    public class TitleServiceTests
    {
        [Theory]
        [MemberData(nameof(TitleServiceTestsMemberData.GetAllTitlesByCatalogNameTestsData), MemberType=typeof(TitleServiceTestsMemberData))]
        public async Task GetAllTitlesByCatalogNameTest(string name)
        {
            var dbContext = DbContextUtility.CreateCatalogDbContext(
                nameof(GetAllTitlesByCatalogNameTest),
                titles: CatalogsTestData.Titles);
            var service = new TitleService(dbContext, MapperUtility.Mapper);

            var expected = CatalogModelsTestData.Titles
                .Where(title => title.Name == name);
            var actual = await service.Get(name);

            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        public async Task TryGetTitlesByDoesNotExistCatalogIdTest()
        {
            var dbContext = DbContextUtility.CreateCatalogDbContext(
                    nameof(TryGetTitlesByDoesNotExistCatalogIdTest),
                    titles: CatalogsTestData.Titles);
            var service = new TitleService(dbContext, MapperUtility.Mapper);

            var actual = await service.Get(string.Empty);

            Assert.Empty(actual);
        }

        [Theory]
        [MemberData(nameof(TitleServiceTestsMemberData.GetByTitleIdTest), MemberType=typeof(TitleServiceTestsMemberData))]
        public async Task GetByTitleIdTest(Guid id)
        {
            var dbContext = DbContextUtility.CreateCatalogDbContext(
                    nameof(GetByTitleIdTest),
                    titles: CatalogsTestData.Titles);
            var service = new TitleService(dbContext, MapperUtility.Mapper);

            var actual = await service.GetById(id);

            Assert.Equal(id, actual.TitleId);
        }

        [Fact]
        public async Task TryGetByDoesNotExistTitleIdTest()
        {
            var dbContext = DbContextUtility.CreateCatalogDbContext(
                    nameof(TryGetByDoesNotExistTitleIdTest),
                    catalogs: CatalogsTestData.Catalogs);
            var service = new TitleService(dbContext, MapperUtility.Mapper);

            var actual = await service.GetById(Guid.NewGuid());

            Assert.Null(actual);
        }
    }

    public class TitleServiceTestsMemberData
    {
        public static IEnumerable<object[]> GetAllTitlesByCatalogNameTestsData =>
            CatalogModelsTestData.Catalogs
                .Select(catalog => new object[] { catalog.Name });

        public static IEnumerable<object[]> GetByTitleIdTest =>
            CatalogModelsTestData.Titles
                .Select(title => new object[] { title.TitleId });
    }
}
