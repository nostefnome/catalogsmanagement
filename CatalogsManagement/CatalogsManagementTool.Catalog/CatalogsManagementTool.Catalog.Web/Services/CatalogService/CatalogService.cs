﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Data;
using CatalogsManagementTool.Core.Logging;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Web.Services.CatalogService;

public class CatalogService : ICatalogService
{
    private readonly CatalogDbContext _context;
    private readonly IMapper _mapper;

    public CatalogService(CatalogDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    [Log]
    public async Task<IEnumerable<CatalogModel>> GetAll()
    {
        var catalogs = await _context.Catalogs.ToListAsync();
        return catalogs
            .Select(catalog => _mapper.Map<CatalogModel>(catalog));
    }

    [Log]
    public async Task<CatalogModel> GetById(Guid id)
    {
        var catalog = await _context.Catalogs
            .FirstOrDefaultAsync(catalog => catalog.CatalogId == id);

        return _mapper.Map<CatalogModel>(catalog);
    }

    [Log]
    public async Task<CatalogModel> GetByName(string name)
    {
        var catalog = await _context.Catalogs
            .FirstOrDefaultAsync(catalog => catalog.Name == name);

        return _mapper.Map<CatalogModel>(catalog);
    }

    [Log]
    public async Task<Guid> Add(CatalogModel model)
    {
        var entity = _mapper.Map<Data.Entities.Catalog>(model);
        
        await _context.Catalogs.AddAsync(entity);
        await _context.SaveChangesAsync();

        return entity.CatalogId;
    }

    [Log]
    public async Task Update(CatalogModel model)
    {
        var entity = _mapper.Map<Data.Entities.Catalog>(model);

        _context.Catalogs.Update(entity);
        await _context.SaveChangesAsync();
    }

    [Log]
    public async Task Remove(Guid id)
    {
        var entity = _context.Catalogs.First(catalog => catalog.CatalogId == id);
        _context.Catalogs.Remove(entity);

        await _context.SaveChangesAsync();
    }
}
