﻿using CatalogsManagementTool.Catalog.Contract.Models;

namespace CatalogsManagementTool.Catalog.Web.Services.CatalogService;

public interface ICatalogService
{
    Task<CatalogModel> GetById(Guid id);
    Task<CatalogModel> GetByName(string name);
    Task<Guid> Add(CatalogModel model);
    Task<IEnumerable<CatalogModel>> GetAll();
    Task Update(CatalogModel model);
    Task Remove(Guid id);
}
