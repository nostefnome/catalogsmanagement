﻿using CatalogsManagementTool.Catalog.Contract.Models;

namespace CatalogsManagementTool.Catalog.Web.Services.TitleService;

public interface ITitleService
{
    Task<Guid> Add(TitleModel title);
    Task<IEnumerable<TitleModel>> Get(string catalogName = null);
    Task<TitleModel> GetById(Guid id);
    Task<TitleModel> GetByName(string name);
    Task Update(TitleModel title);
    Task Remove(Guid id);
}
