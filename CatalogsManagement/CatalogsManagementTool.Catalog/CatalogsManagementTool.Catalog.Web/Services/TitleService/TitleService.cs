﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Contract.Requests;
using CatalogsManagementTool.Catalog.Data;
using CatalogsManagementTool.Catalog.Data.Entities;
using CatalogsManagementTool.Core.Logging;
using Microsoft.EntityFrameworkCore;

namespace CatalogsManagementTool.Catalog.Web.Services.TitleService;

public class TitleService : ITitleService
{
    private readonly CatalogDbContext _context;
    private readonly IMapper _mapper;

    public TitleService(CatalogDbContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }

    [Log]
    public async Task<Guid> Add(TitleModel title)
    {
        var entity = _mapper.Map<Title>(title);

        await _context.Titles.AddAsync(entity);
        await _context.SaveChangesAsync();

        return entity.TitleId;
    }

    [Log]
    public async Task<IEnumerable<TitleModel>> Get(string catalogName = null)
    {
        var query = _context.Titles.AsQueryable<Title>();

        if (catalogName is not null)
        {
            query = query
                .Where(title => title.Catalog.Name == catalogName);
        }

        return (await query.ToListAsync())
            .Select(title => _mapper.Map<TitleModel>(title));
    }

    [Log]
    public async Task<TitleModel> GetById(Guid id)
    {
        var title = await _context.Titles
            .FirstOrDefaultAsync(title => title.TitleId == id);

        return _mapper.Map<TitleModel>(title);
    }

    [Log]
    public async Task<TitleModel> GetByName(string name)
    {
        var title = await _context.Titles
            .FirstOrDefaultAsync(title => title.Name == name);

        return _mapper.Map<TitleModel>(title);
    }

    [Log]
    public async Task Remove(Guid id)
    {
        var title = await _context.Titles
            .FirstAsync(title => title.TitleId == id);

        _context.Titles.Remove(title);
        await _context.SaveChangesAsync();
    }

    [Log]
    public async Task Update(TitleModel title)
    {
        var entity = _mapper.Map<Title>(title);

        _context.Titles.Update(entity);
        await _context.SaveChangesAsync();
    }
}
