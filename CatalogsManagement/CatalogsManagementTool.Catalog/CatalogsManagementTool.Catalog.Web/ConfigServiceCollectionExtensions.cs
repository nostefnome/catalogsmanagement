﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Data;
using CatalogsManagementTool.Catalog.Web.Services.CatalogService;
using CatalogsManagementTool.Catalog.Web.Services.TitleService;
using CatalogsManagementTool.Core.Settings.LoggingSettings;
using CorrelationId.DependencyInjection;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace CatalogsManagementTool.Catalog.Web;

public static class ConfigServiceCollectionExtensions
{
    public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration config, IWebHostEnvironment env)
    {
        services.AddControllers();
        services.AddFluentValidation(options =>
        {
            options.RegisterValidatorsFromAssemblyContaining<ICatalogClient>();
            options.ImplicitlyValidateChildProperties = true;
            options.LocalizationEnabled = false;
            options.DisableDataAnnotationsValidation = true;
        });

        if (env.IsProduction())
        {
            services.AddDbContext<CatalogDbContext>(options => options
                .UseLazyLoadingProxies()
                .UseSqlServer(config.GetConnectionString("DefaultConnection")));
        }
        else
        {
            services.AddDbContext<CatalogDbContext>(options => options
               .UseLazyLoadingProxies()
               .UseNpgsql(config.GetConnectionString("DefaultConnection")));
        }
        
        services.AddScoped<ICatalogService, CatalogService>();
        services.AddScoped<ITitleService, TitleService>();

        services.AddAutoMapper(typeof(AutoMapperProfile));

        services.AddDefaultCorrelationId(options =>
            CorrelationIdSettings.Configure(options));
        services.AddHttpLogging(options =>
            HttpLoggingSettings.Configure(options));

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "CatalogsManagementTool.Catalog",
                Version = "v1"
            });
            c.ExampleFilters();
            c.CustomSchemaIds(type => type.ToString());
            c.EnableAnnotations();
        });
        services.AddSwaggerExamples();

        return services;
    }
}
