#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src
COPY ["CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Web/CatalogsManagementTool.Catalog.Web.csproj", "CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Web/"]
COPY ["CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Contract/CatalogsManagementTool.Catalog.Contract.csproj", "CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Contract/"]
COPY ["CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Data/CatalogsManagementTool.Catalog.Data.csproj", "CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Data/"]
COPY ["CatalogsManagementTool.Core/CatalogsManagementTool.Core.csproj", "CatalogsManagementTool.Core/"]
RUN dotnet restore "CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Web/CatalogsManagementTool.Catalog.Web.csproj"
COPY . .
WORKDIR "/src/CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Web"

RUN dotnet dev-certs https -ep aspnetapp.pfx -p crypticpassword

RUN dotnet build "CatalogsManagementTool.Catalog.Web.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "CatalogsManagementTool.Catalog.Web.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app

COPY --from=publish /app/publish .
COPY --from=build /src/CatalogsManagementTool.Catalog/CatalogsManagementTool.Catalog.Web/aspnetapp.pfx aspnetapp.pfx

ENV ASPNETCORE_Kestrel__Certificates__Default__Path=/app/aspnetapp.pfx
ENV ASPNETCORE_Kestrel__Certificates__Default__Password=crypticpassword

ENTRYPOINT ["dotnet", "CatalogsManagementTool.Catalog.Web.dll"]
