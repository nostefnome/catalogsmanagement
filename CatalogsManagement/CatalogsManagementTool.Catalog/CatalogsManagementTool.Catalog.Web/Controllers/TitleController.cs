﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Contract.Requests;
using CatalogsManagementTool.Catalog.Web.Services.TitleService;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace CatalogsManagementTool.Catalog.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TitleController : Controller
{
    private readonly ITitleService _titleService;
    private readonly IMapper _mapper;

    public TitleController(
        ITitleService titleService,
        IMapper mapper)
    {
        _titleService = titleService;
        _mapper = mapper;
    }

    [HttpGet("get")]
    [SwaggerResponse(200, Type = typeof(IEnumerable<TitleModel>))]
    public async Task<IActionResult> GetByCatalogName([FromQuery] string catalogName = null)
    {
        var result = await _titleService.Get(catalogName);
        return Ok(result);
    }

    [HttpGet("get/{name}")]
    [SwaggerResponse(200, Type = typeof(TitleModel))]
    public async Task<IActionResult> GetTitleByName([FromRoute] string name)
    {
        var result = await _titleService.GetByName(name);
        return Ok(result);
    }

    [HttpPost("add")]
    [SwaggerResponse(200, Type = typeof(Guid))]
    [SwaggerResponse(400, Description =
        "Catalog with id {CatalogId} does not exist\n\r" +
        "Title with name {name} already exists\n\r" +
        "Title count must be more than zero")]
    public async Task<IActionResult> Add([FromBody] AddTitleRequest request)
    {
        var model = _mapper.Map<TitleModel>(request);
        var result = await _titleService.Add(model);

        return Ok(result);
    }

    [HttpPut("update")]
    [SwaggerResponse(200)]
    [SwaggerResponse(400, Description =
        "Catalog with id {CatalogId} does not exist\n\r" +
        "Title count must be more than zero\n\r" +
        "Tile with id {title} does not exist")]
    public async Task<IActionResult> Update([FromBody] UpdateTitleRequest request)
    {
        var model = _mapper.Map<TitleModel>(request);
        await _titleService.Update(model);

        return Ok();
    }

    [HttpDelete("remove/{titleId:guid}")]
    [SwaggerResponse(200)]
    public async Task<IActionResult> Remove([FromRoute] Guid titleId)
    {
        await _titleService.Remove(titleId);
        return Ok();
    }
}
