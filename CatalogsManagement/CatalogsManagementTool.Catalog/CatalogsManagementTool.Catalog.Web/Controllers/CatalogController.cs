﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Contract.Requests;
using CatalogsManagementTool.Catalog.Web.Services.CatalogService;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace CatalogsManagementTool.Catalog.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CatalogController : Controller
{
    private readonly ICatalogService _catalogService;
    private readonly IMapper _mapper;

    public CatalogController(ICatalogService catalogService, IMapper mapper)
    {
        _catalogService = catalogService;
        _mapper = mapper;
    }

    [HttpGet("get/all")]
    [SwaggerResponse(200, Type = typeof(IEnumerable<CatalogModel>))]
    public async Task<IActionResult> GetAll()
    {
        var result = await _catalogService.GetAll();
        return Ok(result);
    }

    [HttpGet("get/{name}")]
    [SwaggerResponse(200, Type = typeof(CatalogModel))]
    public async Task<IActionResult> Get([FromRoute] string name)
    {
        var result = await _catalogService.GetByName(name);
        return Ok(result);
    }

    [HttpPost("add")]
    [SwaggerResponse(200, Type = typeof(Guid))]
    [SwaggerResponse(400, Description = "Catalog with name {name} already exists")]
    public async Task<IActionResult> Add([FromBody] AddCatalogRequest request)
    {
        var result = await _catalogService.Add(new CatalogModel
        {
            Name = request.Name
        });
        return Ok(result);
    }

    [HttpPut("update")]
    [SwaggerResponse(200)]
    [SwaggerResponse(400, Description =
        "Catalog with id {CatalogId} does not exist\n\r")]
    public async Task<IActionResult> Update([FromBody] UpdateCatalogRequest request)
    {
        var model = _mapper.Map<CatalogModel>(request);
        await _catalogService.Update(model);

        return Ok();
    }

    [HttpDelete("remove/{catalogId:guid}")]
    [SwaggerResponse(200)]
    public async Task<IActionResult> Remove([FromRoute] Guid catalogId)
    {
        await _catalogService.Remove(catalogId);
        return Ok();
    }
}