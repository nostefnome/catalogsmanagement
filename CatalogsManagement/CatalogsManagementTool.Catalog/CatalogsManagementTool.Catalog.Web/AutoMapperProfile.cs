﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract.Models;
using CatalogsManagementTool.Catalog.Contract.Requests;

namespace CatalogsManagementTool.Catalog.Web
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Data.Entities.Catalog, CatalogModel>();
            CreateMap<CatalogModel, Data.Entities.Catalog>();
            CreateMap<Data.Entities.Title, TitleModel>();
            CreateMap<TitleModel, Data.Entities.Title>();

            CreateMap<AddCatalogRequest, CatalogModel>();
            CreateMap<UpdateCatalogRequest, CatalogModel>();
            CreateMap<AddTitleRequest, TitleModel>();
            CreateMap<UpdateTitleRequest, TitleModel>();
        }
    }
}
