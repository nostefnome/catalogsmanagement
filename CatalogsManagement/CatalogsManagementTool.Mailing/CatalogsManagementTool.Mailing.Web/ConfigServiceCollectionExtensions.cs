﻿using CatalogsManagementTool.Core.Settings.LoggingSettings;
using CatalogsManagementTool.Mailing.Web.Services.AdministrationNotificationServices;
using CorrelationId.DependencyInjection;
using System.Text.Json.Serialization;
using Swashbuckle.AspNetCore.Filters;
using VkNet;
using VkNet.Model;
using Microsoft.OpenApi.Models;

namespace CatalogsManagementTool.Mailing.Web;

public static class ConfigServiceCollectionExtensions
{
    public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration config)
    {
        services
            .AddControllers()
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumMemberConverter());
            });

        services.AddDefaultCorrelationId(options =>
            CorrelationIdSettings.Configure(options));
        services.AddHttpLogging(options =>
            HttpLoggingSettings.Configure(options));

        var vkApi = new VkApi();
        vkApi.Authorize(new ApiAuthParams
        {
            AccessToken = config["AccessToken"]
        });

        services.AddScoped<IAdministrationNotificationService>(services =>
            new AdministrationNotificationService(
                vkApi, config.GetValue<ulong>("AdministrationDiscussionId")));

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "CatalogsManagementTool.Mailing",
                Version = "v1"
            });
            c.ExampleFilters();
            c.CustomSchemaIds(type => type.FullName.Replace("+", "."));
            c.EnableAnnotations();
        });
        services.AddSwaggerExamples();

        return services;
    }
}
