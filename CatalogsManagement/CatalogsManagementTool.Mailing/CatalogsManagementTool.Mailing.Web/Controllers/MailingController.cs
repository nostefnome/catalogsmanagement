﻿using CatalogsManagementTool.Mailing.Contract.Enums;
using CatalogsManagementTool.Mailing.Contract.Requests;
using CatalogsManagementTool.Mailing.Web.Services.AdministrationNotificationServices;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace CatalogsManagementTool.Mailing.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class MailingController : Controller
{
    private readonly IAdministrationNotificationService _administrationNotificationService;

    public MailingController(IAdministrationNotificationService administrationNotificationService)
    {
        _administrationNotificationService = administrationNotificationService;
    }

    [HttpPost("send")]
    [SwaggerResponse(200)]
    [SwaggerResponse(400, Description = "User role {user role} does not exists")]
    public async Task<IActionResult> Notify([FromBody] NotifyRequest request)
    {
        if (!Enum.IsDefined(request.SendTo))
        {
            return BadRequest($"User role {request.SendTo} does not exists");
        }

        if (request.SendTo == UserRole.Administration)
        {
            await _administrationNotificationService.Notify(request.Notification);
        }
        
        return Ok();
    }
}