﻿using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.Mailing.Contract.Enums;
using CatalogsManagementTool.Mailing.Contract.Requests;
using VkNet.Abstractions;
using VkNet.Model.RequestParams;

namespace CatalogsManagementTool.Mailing.Web.Services.AdministrationNotificationServices;

public class AdministrationNotificationService : IAdministrationNotificationService
{
    private readonly IVkApi _vkApi;
    private readonly ulong _id;

    public AdministrationNotificationService(IVkApi vkApi, ulong id)
    {
        _vkApi = vkApi;
        _id = id;
    }

    [Log]
    public async Task Notify(Body body)
    {
        var prefix = ConvertNotificationLevelToHumanReadableString(body.Level);

        await _vkApi.Messages.SendAsync(new MessagesSendParams
        {
            RandomId = DateTime.Now.Ticks,
            PeerId = (long?)_id,
            Message = 
                $"[{prefix}]\n\r" +
                $"{body.Message}"
        });
    }

    private string ConvertNotificationLevelToHumanReadableString(NotificationLevelEnum level)
    {
        switch(level)
        {
            case NotificationLevelEnum.Information: return "Information";
            case NotificationLevelEnum.Warning: return "Warning";
            case NotificationLevelEnum.Error: return "Error";
            default: return "unknown";
        }
    }
}
