﻿using CatalogsManagementTool.Mailing.Contract.Requests;

namespace CatalogsManagementTool.Mailing.Web.Services.AdministrationNotificationServices;

public interface IAdministrationNotificationService
{
    Task Notify(Body body);
}
