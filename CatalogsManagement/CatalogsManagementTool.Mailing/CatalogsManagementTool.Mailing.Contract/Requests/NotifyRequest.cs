﻿using CatalogsManagementTool.Mailing.Contract.Enums;

namespace CatalogsManagementTool.Mailing.Contract.Requests;

public class NotifyRequest
{
    public UserRole SendTo { get; set; }
    public Body Notification { get; set; }
}

public class Body
{
    public NotificationLevelEnum Level { get; set; }
    public string Message { get; set; }
}
