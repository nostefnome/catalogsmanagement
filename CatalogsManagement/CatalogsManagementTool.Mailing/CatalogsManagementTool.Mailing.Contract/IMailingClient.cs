﻿using CatalogsManagementTool.Mailing.Contract.Requests;
using RestEase;

namespace CatalogsManagementTool.Mailing.Contract;

public interface IMailingClient
{
    [Post("api/mailing/send")]
    Task Notify([Body] NotifyRequest request);
}
