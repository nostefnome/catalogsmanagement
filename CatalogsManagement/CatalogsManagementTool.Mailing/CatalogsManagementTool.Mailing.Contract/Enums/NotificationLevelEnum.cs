﻿namespace CatalogsManagementTool.Mailing.Contract.Enums;

public enum NotificationLevelEnum
{
    Information,
    Warning,
    Error
}