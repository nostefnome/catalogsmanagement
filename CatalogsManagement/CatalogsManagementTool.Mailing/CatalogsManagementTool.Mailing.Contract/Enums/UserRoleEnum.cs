﻿namespace CatalogsManagementTool.Mailing.Contract.Enums;

public enum UserRole
{
    Administration,
    Subscriber
}
