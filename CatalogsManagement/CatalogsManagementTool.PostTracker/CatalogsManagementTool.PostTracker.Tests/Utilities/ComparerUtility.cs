﻿using FluentAssertions.Equivalency;
using FluentAssertions.Execution;

namespace CatalogsManagementTool.PostTracker.Tests.Utilities
{
    internal static class ComparerUtility
    {
        public static void CompareStringsNullOrEmpty(IAssertionContext<string> ctx)
        {
            var equal = (ctx.Subject ?? string.Empty)
                .Equals(ctx.Expectation ?? string.Empty);

            Execute.Assertion
                .BecauseOf(ctx.Because, ctx.BecauseArgs)
                .ForCondition(equal)
                .FailWith("Expected {context:string} to be {0} {reason}, but found {1}", ctx.Subject, ctx.Expectation);
        }
    }
}
