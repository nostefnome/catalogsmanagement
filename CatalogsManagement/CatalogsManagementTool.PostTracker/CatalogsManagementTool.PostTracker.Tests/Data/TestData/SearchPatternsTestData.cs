﻿using CatalogsManagementTool.PostTracker.Contract.Models;
using System.Collections.Generic;
using System.Linq;

namespace CatalogsManagementTool.PostTracker.Tests.Data.TestData
{
    internal static class SearchPatternsTestData
    {
        public static readonly IEnumerable<SearchPattern> SearchFullTitlePatterns = new[]
        {
            new SearchPattern
            {
                Catalog = "(?<tag>^Аниме|Манга)\\s*:",
                Name = "(?:Аниме|Манга)\\s*:\\s*(?<tag>[\\s\\w\\d.,:_-]+)/",
                Tag = "(?<tag>#[\\w\\d_]+@tag)",
                TagSign = "tag"
            }
        };

        public static readonly IEnumerable<SearchPattern> SearchPartlyTitlePatterns = new[]
        {
            new SearchPattern
            {
                Catalog = "",
                Name = "#(?<tag>[а-я]+)(\\s+|\\n|)",
                Tag = "(?<tag>#[\\w\\d_]+@tag)",
                TagSign = "tag"
            }
        };

        public static IEnumerable<SearchPattern> AllSearchPatterns =>
            SearchFullTitlePatterns.Concat(SearchPartlyTitlePatterns);
    }
}
