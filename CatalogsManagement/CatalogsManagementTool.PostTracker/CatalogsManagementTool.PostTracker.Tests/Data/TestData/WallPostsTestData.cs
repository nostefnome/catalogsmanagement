﻿using System.Collections.Generic;
using VkNet.Model.GroupUpdate;

namespace CatalogsManagementTool.PostTracker.Tests.Data.TestData
{
    internal static class WallPostsTestData
    {
        public static IEnumerable<WallPost> WallPostsFullCases = new[]
        {
            new WallPost
            {
                Text = "Аниме: some new: text / some second text \n#tag1@tag #tag2@tag",
            },
            new WallPost
            {
                Text = "Аниме: some new, text / some second, text \n#tag1@tag #tag2@tag"
            },
            new WallPost
            {
                Text = "Манга: some new: text / some second text \n#tag1@tag #tag2@tag",
            },
            new WallPost
            {
                Text = "Манга: some new, text / some second, text \n#tag1@tag #tag2@tag"
            }
        };

        public static IEnumerable<WallPost> WallPostsPartlyCases= new[]
        {
            new WallPost
            {
                Text = "#tag1 #tag2@tag #имя",
            },
            new WallPost
            {
                Text = "#tag2 #tag2@tag #имя"
            }
        };

        public static IEnumerable<WallPost> WallPostsNotTitleCases = new[]
        {
            new WallPost
            {
                Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
            }
        };
    }
}
