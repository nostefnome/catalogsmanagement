﻿using CatalogsManagementTool.PostTracker.Web.Parsers;
using System.Collections.Generic;

namespace CatalogsManagementTool.PostTracker.Tests.Data.ExpectedData;

internal static class TitleRecordsExpectedData
{
    public static IEnumerable<TitleRecord> FullTitleRecordsExpected = new[]
    {
        new TitleRecord
        {
            Catalog = "Аниме",
            Name = "some new: text",
            Tag = "#tag1@tag"
        },
        new TitleRecord
        {
            Catalog = "Аниме",
            Name = "some new, text",
            Tag = "#tag1@tag"
        },
        new TitleRecord
        {
            Catalog = "Манга",
            Name = "some new: text",
            Tag = "#tag1@tag"
        },
        new TitleRecord
        {
            Catalog = "Манга",
            Name = "some new, text",
            Tag = "#tag1@tag"
        }
    };

    public static IEnumerable<TitleRecord> PartlyTitleRecordsExpected = new[]
    {
        new TitleRecord
        {
            Name = "имя",
            Tag = "#tag2@tag"
        },
        new TitleRecord
        {
            Name = "имя",
            Tag = "#tag2@tag"
        }
    };
}
