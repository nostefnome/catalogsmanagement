﻿using CatalogsManagementTool.PostTracker.Tests.Data.ExpectedData;
using CatalogsManagementTool.PostTracker.Tests.Data.TestData;
using CatalogsManagementTool.PostTracker.Tests.Utilities;
using CatalogsManagementTool.PostTracker.Web.Parsers;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using VkNet.Model.GroupUpdate;
using Xunit;

namespace CatalogsManagementTool.PostTracker.Tests.Parsers;

public class PostParserTests
{
    [Theory]
    [MemberData(nameof(PostParserTestsMemberData.ParseFullTitlePostWithFullTitlePatternsTest), 
        MemberType = typeof(PostParserTestsMemberData))]
    public void ParseFullTitlePostWithFullTitlePatternsTest(WallPost post, TitleRecord expected)
    {
        var parser = new PostParser(SearchPatternsTestData.SearchFullTitlePatterns);
        var actual = parser.Parse(post);

        actual.Should().BeEquivalentTo(expected);
    }

    [Theory]
    [MemberData(nameof(PostParserTestsMemberData.ParsePartlyTitlePostWithPartlyTitlePaternsTest), 
        MemberType = typeof(PostParserTestsMemberData))]
    public void ParsePartlyTitlePostWithPartlyTitlePaternsTest(WallPost post, TitleRecord expected)
    {
        var parser = new PostParser(SearchPatternsTestData.SearchPartlyTitlePatterns);
        var actual = parser.Parse(post);

        actual.Should().BeEquivalentTo(expected, options => options
            .Using<string>(x => ComparerUtility.CompareStringsNullOrEmpty(x))
            .WhenTypeIs<string>());
    }

    [Theory]
    [MemberData(nameof(PostParserTestsMemberData.ParseFullAndPartlyTitlesWithAllPatterns), 
        MemberType = typeof(PostParserTestsMemberData))]
    public void ParseFullAndPartlyTitlesWithAllPatterns(WallPost post, TitleRecord expected)
    {
        var parser = new PostParser(SearchPatternsTestData.AllSearchPatterns);
        var actual = parser.Parse(post);

        actual.Should().BeEquivalentTo(expected, options => options
            .Using<string>(x => ComparerUtility.CompareStringsNullOrEmpty(x))
            .WhenTypeIs<string>());
    }
}

public class PostParserTestsMemberData
{
    public static IEnumerable<object[]> ParseFullTitlePostWithFullTitlePatternsTest =>
        WallPostsTestData.WallPostsFullCases
            .Zip(TitleRecordsExpectedData.FullTitleRecordsExpected)
            .Select(pair => new object[] { pair.First, pair.Second });

    public static IEnumerable<object[]> ParsePartlyTitlePostWithPartlyTitlePaternsTest => 
        WallPostsTestData.WallPostsPartlyCases
            .Zip(TitleRecordsExpectedData.PartlyTitleRecordsExpected)
            .Select(pair => new object[] { pair.First, pair.Second });

    public static IEnumerable<object[]> ParseFullAndPartlyTitlesWithAllPatterns =>
        WallPostsTestData.WallPostsFullCases
            .Concat(WallPostsTestData.WallPostsPartlyCases)
            .Zip(TitleRecordsExpectedData.FullTitleRecordsExpected
                .Concat(TitleRecordsExpectedData.PartlyTitleRecordsExpected))
            .Select(pair => new object[] { pair.First, pair.Second });
}
