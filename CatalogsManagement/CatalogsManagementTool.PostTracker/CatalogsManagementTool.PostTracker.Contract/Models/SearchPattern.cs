﻿namespace CatalogsManagementTool.PostTracker.Contract.Models;

public class SearchPattern
{
    public string Catalog { get; set; }
    public string Name { get; set; }
    public string Tag { get; set; }

    public string TagSign { get; set; }
}
