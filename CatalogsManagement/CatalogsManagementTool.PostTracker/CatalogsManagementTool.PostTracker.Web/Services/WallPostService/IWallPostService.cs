﻿using VkNet.Model.GroupUpdate;

namespace CatalogsManagementTool.PostTracker.Web.Services.WallPostService;

public interface IWallPostService
{
    Task TrackNewPost(WallPost post);
}
