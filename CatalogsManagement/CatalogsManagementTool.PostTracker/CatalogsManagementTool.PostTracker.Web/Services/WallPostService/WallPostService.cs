﻿using CatalogsManagementTool.Mailing.Contract;
using CatalogsManagementTool.Mailing.Contract.Enums;
using CatalogsManagementTool.Mailing.Contract.Requests;
using CatalogsManagementTool.PostTracker.Web.Parsers;
using CatalogsManagementTool.PostTracker.Web.Services.TitleService;
using VkNet.Model.GroupUpdate;
using static CatalogsManagementTool.Mailing.Contract.Requests.NotifyRequest;

namespace CatalogsManagementTool.PostTracker.Web.Services.WallPostService;

public class WallPostService : IWallPostService
{
    private readonly ITitleService _titleService;
    private readonly IPostParser _postParser;
    private readonly IMailingClient _notificatorClient;

    public WallPostService(
        ITitleService titleService,
        IPostParser postParser,
        IMailingClient notificatorClient)
    {
        _titleService = titleService;
        _postParser = postParser;
        _notificatorClient = notificatorClient;
    }

    public async Task TrackNewPost(WallPost post)
    {
        var titleRecord = _postParser.Parse(post);
        var titleAdded = await _titleService.TryAdd(titleRecord);

        var request = titleAdded ?
            new NotifyRequest
            {
                SendTo = UserRole.Administration,
                Notification = new Body
                {
                    Level = NotificationLevelEnum.Information,
                    Message = BuildSuccessMessage(post, titleRecord)
                }
            } : 
            new NotifyRequest
            {
                SendTo = UserRole.Administration,
                Notification = new Body
                {
                    Level = NotificationLevelEnum.Warning,
                    Message = BuildFailMessage(post, titleRecord)
                    
                }
            };


        await _notificatorClient.Notify(request);
    }

    private string BuildFailMessage(WallPost post, TitleRecord record)
    {
        return
            $"Post {BuildPostUrl(post)} wasn't added to catalogs \n\r" +
            $"{BuildExtractedRecordString(record)}";
    }

    private string BuildSuccessMessage(WallPost post, TitleRecord record)
    {
        return
            $"Post {BuildPostUrl(post)} was added to catalogs\n\r" +
            $"{BuildExtractedRecordString(record)}";
    }

    private string BuildPostUrl(WallPost post)
    {
        return $"https://vk.com/club{-post.OwnerId}?w=wall{post.OwnerId}_{post.Id}/all";
    }

    private string BuildExtractedRecordString(TitleRecord record)
    {
        return
            $"Extracted fields:\n\r" +
            $"  Catalog: {record.Catalog};\n\r" +
            $"  Name: {record.Name};\n\r" +
            $"  Tag: {record.Tag}.\n\r";
    }
}
