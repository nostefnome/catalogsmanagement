﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Requests;
using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.PostTracker.Web.Parsers;

namespace CatalogsManagementTool.PostTracker.Web.Services.TitleService;

public class TitleService : ITitleService
{
    private readonly ICatalogClient _catalogClient;

    public TitleService(ICatalogClient catalogClient)
    {
        _catalogClient = catalogClient;
    }

    [Log]
    public async Task<bool> TryAdd(TitleRecord record)
    {
        if (IsEmpty(record) || IsNotFull(record))
        {
            return false;
        }

        var title = await _catalogClient.GetTitleByName(record.Name);

        if (title is null)
        {
            await _catalogClient.AddTitle(new AddTitleRequest
            {
                CatalogId = await GetCatalogIdByNameOrCreateNewIfNotExists(record.Catalog),
                Name = record.Name,
                Tag = record.Tag,
                Count = 1
            });
        }
        else
        {
            title.Count++;
            await _catalogClient.UpdateTitle(new UpdateTitleRequest
            {
                CatalogId = title.CatalogId,
                TitleId = title.TitleId,
                Name = title.Name,
                Tag = title.Tag,
                Count = title.Count,
            });
        }

        return true;
    }

    private async Task<Guid> GetCatalogIdByNameOrCreateNewIfNotExists(string name)
    {
        return (
            await _catalogClient.GetCatalogByName(name))?.CatalogId ??
            await _catalogClient.AddCatalog(new AddCatalogRequest
            {
                Name = name
            });
    }

    private bool IsEmpty(TitleRecord title)
    {
        return TitleRecordUtility.CountNonNullFields(title) == 0;
    }

    private bool IsNotFull(TitleRecord title)
    {
        return TitleRecordUtility.CountNonNullFields(title) != 3;
    }
}
