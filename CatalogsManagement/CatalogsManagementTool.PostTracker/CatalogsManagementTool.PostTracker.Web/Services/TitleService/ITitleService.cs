﻿using CatalogsManagementTool.PostTracker.Web.Parsers;

namespace CatalogsManagementTool.PostTracker.Web.Services.TitleService;

public interface ITitleService
{
    Task<bool> TryAdd(TitleRecord record);
}
