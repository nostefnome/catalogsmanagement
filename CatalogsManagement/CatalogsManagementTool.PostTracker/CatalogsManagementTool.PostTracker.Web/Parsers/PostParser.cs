﻿using CatalogsManagementTool.Core.Logging;
using CatalogsManagementTool.PostTracker.Contract.Models;
using CatalogsManagementTool.PostTracker.Web.Services.TitleService;
using System.Text.RegularExpressions;
using VkNet.Model.GroupUpdate;

namespace CatalogsManagementTool.PostTracker.Web.Parsers;

public class PostParser : IPostParser
{
    private readonly IEnumerable<SearchPattern> _searchPatterns;

    public PostParser(IEnumerable<SearchPattern> searchPatterns)
    {
        _searchPatterns = searchPatterns;
    }

    [Log]
    public TitleRecord Parse(WallPost post)
    {
        var record = _searchPatterns
            .Select(pattern => new TitleRecord
            {
                Catalog = TryFindField(pattern.Catalog, pattern.TagSign, post.Text),
                Name = TryFindField(pattern.Name, pattern.TagSign, post.Text),
                Tag = TryFindField(pattern.Tag, pattern.TagSign, post.Text)
            })
            .OrderByDescending(title => TitleRecordUtility.CountNonNullFields(title))
            .First();
        record.Name = record.Name.Trim();

        return record;
    }

    private string TryFindField(string pattern, string tagSign, string text)
    {
        return Regex.Match(text, pattern, RegexOptions.IgnoreCase)
            .Groups[tagSign]
            .Value;
    }
}