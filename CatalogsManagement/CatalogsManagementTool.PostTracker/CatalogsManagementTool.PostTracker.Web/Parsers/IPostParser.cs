﻿using VkNet.Model.GroupUpdate;

namespace CatalogsManagementTool.PostTracker.Web.Parsers;

public class TitleRecord
{
    public string Catalog { get; set; }
    public string Name { get; set; }
    public string Tag { get; set; }
}

public interface IPostParser
{
    TitleRecord Parse(WallPost post);
}
