using CatalogsManagementTool.Core.Settings;
using CatalogsManagementTool.Core.Settings.LoggingSettings;
using CatalogsManagementTool.PostTracker.Web;
using CorrelationId;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration((context, config) =>
    GlobalConfigurationSettings.Configure(context, config));
builder.Host.UseSerilog((context, loggerConfiguration) =>
    SerilogLoggingSettings.ConfigureSerilogLogger(context, loggerConfiguration));

builder.Services.AddConfig(builder.Configuration);

var app = builder.Build();
var env = app.Environment;

if (env.EnvironmentName == "IsolatedDevelopment" || env.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseSwagger();
app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CatalogsManagementTool.PostTracker v1"));

app.UseRouting();

app.UseCorrelationId();
app.UseHttpLogging();
app.UseSerilogRequestLogging(options =>
{
    SerilogLoggingSettings.ConfigureSerilogRequestLogging(options);
});

//app.UseAuthentication();
//app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();