﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Core.Models.Vk;
using CatalogsManagementTool.Core.Settings.LoggingSettings;
using CatalogsManagementTool.Mailing.Contract;
using CatalogsManagementTool.PostTracker.Contract.Models;
using CatalogsManagementTool.PostTracker.Web.Parsers;
using CatalogsManagementTool.PostTracker.Web.Services.TitleService;
using CatalogsManagementTool.PostTracker.Web.Services.WallPostService;
using CorrelationId.DependencyInjection;
using CorrelationId.HttpClient;
using Microsoft.OpenApi.Models;
using RestEase.HttpClientFactory;
using Swashbuckle.AspNetCore.Filters;
using System.Text.Json.Serialization;

namespace CatalogsManagementTool.PostTracker.Web;

public static class ConfigServiceCollectionExtensions
{
    public static IServiceCollection AddConfig(this IServiceCollection services, IConfiguration config)
    {
        services
            .AddControllers()
            .AddNewtonsoftJson()
            .AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumMemberConverter());
            });

        services.AddDefaultCorrelationId(options => 
            CorrelationIdSettings.Configure(options));
        services.AddHttpLogging(options => 
            HttpLoggingSettings.Configure(options));

        services.AddRestEaseClient<ICatalogClient>(config["Urls:Catalog"])
            .AddCorrelationIdForwarding();
        services.AddRestEaseClient<IMailingClient>(config["Urls:Mailing"])
            .AddCorrelationIdForwarding();

        services.AddScoped(services => new VkConfirmation
        {
            ConfirmationValue = config["Confirmation"]
        });

        services.AddScoped<IPostParser>(services =>
            new PostParser(config.GetSection("SearchPatterns").Get<IEnumerable<SearchPattern>>()));
        services.AddScoped<ITitleService, TitleService>();
        services.AddScoped<IWallPostService, WallPostService>();

        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo 
            {
                Title = "CatalogsManagementTool.PostTracker", 
                Version = "v1"
            });
            c.ExampleFilters();
            c.CustomSchemaIds(type => type.ToString());
            c.EnableAnnotations();
        });
        services.AddSwaggerExamples();

        return services;
    }
}
