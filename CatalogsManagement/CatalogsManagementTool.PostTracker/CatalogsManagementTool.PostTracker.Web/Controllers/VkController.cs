﻿using Microsoft.AspNetCore.Mvc;
using VkNet.Model.GroupUpdate;
using CatalogsManagementTool.PostTracker.Web.Services.WallPostService;
using CatalogsManagementTool.Core.Models.Vk;
using CatalogsManagementTool.VkCircuitBreaker;

namespace CatalogsManagementTool.PostTracker.Web.Controllers;

[Route("api/vk/wall_post")]
[ApiController]
public class VkController : Controller
{
    private readonly IWallPostService _wallPostService;
    private readonly VkConfirmation _confirmation;

    public VkController(IWallPostService wallPostService, VkConfirmation confirmation)
    {
        _wallPostService = wallPostService;
        _confirmation = confirmation;
    }

    [HttpPost("[action]")]
    [EnsureRequestDoesNotContainRetry]
    public async Task<IActionResult> Callback([FromBody] VkEvent vkEvent)
    {
        if (vkEvent.Type is VkEventType.Confirmation)
        {
            return Ok(_confirmation.ConfirmationValue);
        }

        if (vkEvent.Type is not VkEventType.NewPost)
        {
            return BadRequest($"Type {vkEvent.Type} does not supports.");
        }

        var wallPost = WallPost.FromJson(new (vkEvent.Object));
        await _wallPostService.TrackNewPost(wallPost);
        
        return Ok();
    }
}
