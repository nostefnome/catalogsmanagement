﻿using CatalogsManagementTool.PostTracker.Web.Parsers;

namespace CatalogsManagementTool.PostTracker.Web.Services.TitleService;

static class TitleRecordUtility
{
    public static int CountNonNullFields(TitleRecord title)
    {
        return
            CalculateAffiliation(title.Name) +
            CalculateAffiliation(title.Tag) +
            CalculateAffiliation(title.Catalog);
    }

    private static int CalculateAffiliation(string input)
    {
        return string.IsNullOrEmpty(input) ? 0 : 1;
    }
}
