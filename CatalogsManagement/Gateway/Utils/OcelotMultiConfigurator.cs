using System.Text.Json;

namespace Gateway.Utils;

public static class OcelotMultiConfigurator
{
    public static string MergeConfigurations(string folder, IEnumerable<string> configFilesNames, string globalConfigName, string outputFileName)
    {
        var configFiles = new List<Config>();
        Config globalConfig;

        foreach (var file in configFilesNames)
        {
            using var r = new StreamReader(Path.Combine(folder, file));
            var json = r.ReadToEnd();
            var fileConfig = JsonSerializer.Deserialize<Config>(json);

            configFiles.Add(fileConfig);
        }

        using (var r = new StreamReader(Path.Combine(folder, globalConfigName)))
        {
            var json = r.ReadToEnd();
            globalConfig = JsonSerializer.Deserialize<Config>(json);
        }

        var fullConfig = new Config
        {
            Routes = configFiles
                .SelectMany(x => x.Routes)
                .Union(globalConfig.Routes)
                .ToList(),
            SwaggerEndPoints = configFiles
                .Where(x => x.SwaggerEndPoints != null)
                .SelectMany(x => x.SwaggerEndPoints)
                .Union(globalConfig.SwaggerEndPoints)
                .ToList(),
            GlobalConfiguration = globalConfig.GlobalConfiguration
        };

        using (var w = new StreamWriter(outputFileName))
        {
            var json = JsonSerializer.Serialize(fullConfig);
            w.Write(json);
        }

        return outputFileName;
    }

    private class Config
    {
        public IEnumerable<object> Routes { get; set; }
        public IEnumerable<object> SwaggerEndPoints { get; set; }
        public object GlobalConfiguration { get; set; }
    }
}
