using CatalogsManagementTool.Core.Logging;

namespace Gateway.Middlewares;

public class HeaderCorrelationIdMiddleware
{
    private readonly RequestDelegate _next;

    public HeaderCorrelationIdMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var headers = context.Request.Headers;
        var correlationidExists = headers.ContainsKey(LoggingConsts.CorrelationId);

        if (!correlationidExists)
        {
            var correlationId = GenerateCorrelationId();
            context.Request.Headers.Add(LoggingConsts.CorrelationId, correlationId);
        }

        await _next(context);
    }

    private string GenerateCorrelationId()
    {
        return Guid.NewGuid().ToString();
    }
}

public static class HeaderCorrelationIdMiddlewareExtensions
{
    public static IApplicationBuilder UseHeaderCorrelationId(this IApplicationBuilder builder)
    {
        return builder.UseMiddleware<HeaderCorrelationIdMiddleware>();
    }
}
