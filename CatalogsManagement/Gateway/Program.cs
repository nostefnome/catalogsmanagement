using CatalogsManagementTool.Core.Settings;
using CatalogsManagementTool.Core.Settings.LoggingSettings;
using CorrelationId.DependencyInjection;
using Gateway.Middlewares;
using Gateway.Utils;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.ConfigureAppConfiguration((context, config) =>
{
    GlobalConfigurationSettings.Configure(context, config);

    config.AddJsonFile(
        OcelotMultiConfigurator.MergeConfigurations("Configurations",
        new List<string>
        {
            "ocelot.Catalog.json",
            "ocelot.Mailing.json",
            "ocelot.PostTracker.json",
            "ocelot.MessageTracker.json"
        },
        "ocelot.global.json",
        "ocelot.json"), 
        true, true);
});
builder.Host.UseSerilog((context, loggerConfiguration) =>
    SerilogLoggingSettings.ConfigureSerilogLogger(context, loggerConfiguration));

var configurationManager = builder.Configuration;

builder.Services.AddControllers();
builder.Services.AddOcelot(configurationManager);
builder.Services.AddSwaggerForOcelot(configurationManager);

builder.Services.AddCorrelationId(options => CorrelationIdSettings.Configure(options));
builder.Services.AddHttpLogging(options => HttpLoggingSettings.Configure(options));

var app = builder.Build();
var env = app.Environment;

if (env.EnvironmentName == "IsolatedDevelopment" || env.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}

app.UseSwaggerForOcelotUI(opt =>
{
    opt.PathToSwaggerGenerator = "/swagger/docs";
});

app.UseHeaderCorrelationId();
app.UseHttpLogging();
app.UseSerilogRequestLogging(options => SerilogLoggingSettings.ConfigureSerilogRequestLogging(options));

app.UseOcelot().Wait();

app.UseRouting();

//app.UseAuthentication();
//app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();
