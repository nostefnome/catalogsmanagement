namespace CatalogsManagementTool.Core.Logging;

public class LoggingConsts
{
    public const string AssemblyName = "AssemblyName";
    public const string CorrelationId = "CorrelationId";
    public const string Elapsed = "Elapsed";

    public const string RouteValues = "RouteValues";
    public const string QueryString = "QueryString";

    public const string Args = "Args";
    public const string MethodName = "MethodName";
    public const string MethodResult = "Result";
}
