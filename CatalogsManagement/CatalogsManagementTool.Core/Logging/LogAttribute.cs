using System.Diagnostics;
using MethodBoundaryAspect.Fody.Attributes;
using Newtonsoft.Json;
using Serilog;
using Serilog.Context;

namespace CatalogsManagementTool.Core.Logging;

[AttributeUsage(AttributeTargets.Method)]
public sealed class LogAttribute : OnMethodBoundaryAspect
{
    private static readonly string ExitMessageTemplate = BuildStandartMessageTemplate("Processed");
    private static readonly string ExceptionMessageTemplate = BuildStandartMessageTemplate("Failed");

    private readonly ILogger _logger = Log.ForContext<LogAttribute>();
    private readonly Stopwatch _stopWatch = new();

    public override void OnEntry(MethodExecutionArgs arg)
    {
        _stopWatch.Start();
    }

    public override void OnExit(MethodExecutionArgs arg)
    {
        _stopWatch.Stop();

        if (ContainsException(arg))
        {
            return;
        }

        var containsResult = TryGetResult(arg, out var result);

        using (LogContext.PushProperty(LoggingConsts.AssemblyName, arg.Method.DeclaringType))
        using (LogContext.PushProperty(LoggingConsts.MethodName, arg.Method.Name))
        using (LogContext.PushProperty(LoggingConsts.Args, GetParameterPairs(arg)))
        using (LogContext.PushProperty(LoggingConsts.Elapsed, _stopWatch.Elapsed.Milliseconds))
        {
            if (containsResult)
            {
                using (LogContext.PushProperty(LoggingConsts.MethodResult,
                    JsonConvert.SerializeObject(result, Formatting.Indented)))
                {
                    _logger.Information(ExitMessageTemplate);
                }
            }
            else
            {
                _logger.Information(ExitMessageTemplate);
            }
        }
    }

    public override void OnException(MethodExecutionArgs arg)
    {
        using (LogContext.PushProperty(LoggingConsts.AssemblyName, arg.Method.DeclaringType))
        using (LogContext.PushProperty(LoggingConsts.MethodName, arg.Method.Name))
        using (LogContext.PushProperty(LoggingConsts.Args, GetParameterPairs(arg)))
        using (LogContext.PushProperty(LoggingConsts.Elapsed, _stopWatch.Elapsed.Milliseconds))
        {
            _logger.Error(arg.Exception, ExceptionMessageTemplate);
        }
    }

    private static bool ContainsException(MethodExecutionArgs args)
    {
        return args.Exception != null;
    }

    private bool TryGetResult(MethodExecutionArgs args, out object result)
    {
        if (IsTaskWithResult(args))
        {
            var task = args.ReturnValue as Task;
            result = GetResult(task);

            return task.IsCompletedSuccessfully;
        }
        else if (IsMethodWithResult(args))
        {
            result = args.ReturnValue;
            return true;
        }

        result = default;
        return false;
    }

    private static bool IsTaskWithResult(MethodExecutionArgs args)
    {
        if (args.ReturnValue is Task)
        {
            var type = args.ReturnValue.GetType();
            var properties = type.GetProperties();

            return properties
                .Any(property => property.Name == "Result");
        }

        return false;
    }

    private static bool IsMethodWithResult(MethodExecutionArgs args)
    {
        if (args.ReturnValue is Task)
        {
            return false;
        }

        var methodName = args.Method.Name;
        var method = args.Method.DeclaringType.GetMethod(methodName);
        var returnType = method.ReturnType;

        return
            returnType != typeof(Task) &&
            returnType != typeof(void);
    }

    private static object GetResult(Task task)
    {
        return task
            .GetType()
            .GetProperty("Result")
            .GetValue(task);
    }

    private static Dictionary<string, object> GetParameterPairs(MethodExecutionArgs args)
    {
        return args.Method.GetParameters()
            .Zip(args.Arguments)
            .ToDictionary(
                selector => selector.First.Name,
                selector => selector.Second);
    }

    private static string BuildStandartMessageTemplate(string action)
    {
        return
            $"[{{{LoggingConsts.CorrelationId}}}; {{{LoggingConsts.AssemblyName}}}] " +
            $"{action} " +
            $"{{{LoggingConsts.MethodName}}} " +
            $"in {{{LoggingConsts.Elapsed}:0.0}} ms";
    }
}
