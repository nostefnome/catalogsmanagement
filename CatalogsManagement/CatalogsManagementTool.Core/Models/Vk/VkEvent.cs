﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.Json;

namespace CatalogsManagementTool.Core.Models.Vk;

[Serializable]
public class VkEvent
{
    [JsonProperty("type")]
    public VkEventType Type { get; set; }

    [JsonProperty("group_id")]
    public long Id { get; set; }

    [JsonProperty("event_id")]
    public string EventId { get; set; }

    [JsonProperty("secret")]
    public string? Secret { get; set; } = null;

    [JsonProperty("object")]
    public JObject? Object { get; set; } = null;
}