﻿using System.Runtime.Serialization;

namespace CatalogsManagementTool.Core.Models.Vk;

public enum VkEventType
{
    [EnumMember(Value = "confirmation")]
    Confirmation,
    [EnumMember(Value = "wall_post_new")]
    NewPost,
    [EnumMember(Value = "message_new")]
    NewMessage
}
