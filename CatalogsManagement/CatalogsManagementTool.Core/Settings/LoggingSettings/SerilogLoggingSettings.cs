using CatalogsManagementTool.Core.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.AspNetCore;
using Serilog.Events;
using Serilog.Exceptions;

namespace CatalogsManagementTool.Core.Settings.LoggingSettings;

public static class SerilogLoggingSettings
{
    private static readonly string RequestMessageTemplate =
        $"[{{{LoggingConsts.CorrelationId}}}; {{{LoggingConsts.AssemblyName}}}] " +
        $"{{RequestMethod}} {{RequestPath}} responded {{StatusCode}} in {{Elapsed:0.0000}} ms";

    public static void ConfigureSerilogLogger(HostBuilderContext context, LoggerConfiguration configuration)
    {
        configuration
            .WriteTo.Async(configuration => configuration.Debug())
            .WriteTo.Async(configuration =>
                configuration.Seq(context.Configuration["Urls:Logs"]))
            //TODO: use logstash instead seq in production
            //.WriteTo.Async(configuration => 
            //    configuration.LogstashHttp(context.Configuration["Urls:Logs"]))

            .Enrich.FromLogContext()
            .Enrich.WithExceptionDetails()
            .Enrich.WithAssemblyName()
            .Enrich.WithCorrelationIdHeader(LoggingConsts.CorrelationId)

            .ReadFrom.Configuration(context.Configuration);
    }

    public static void ConfigureSerilogRequestLogging(RequestLoggingOptions options)
    {
        options.MessageTemplate = RequestMessageTemplate;
        options.GetLevel = GetLogLevel;

        options.EnrichDiagnosticContext = (diagnosticsContext, httpContext) =>
        {
            var request = httpContext.Request;
            if (request.QueryString.HasValue)
            {
                diagnosticsContext.Set(LoggingConsts.QueryString, request.QueryString);
            }

            var routeValues = request.RouteValues;
            if (routeValues.Any())
            {
                diagnosticsContext.Set(LoggingConsts.RouteValues, routeValues);
            };
        };
    }

    private static LogEventLevel GetLogLevel(HttpContext context, double _, Exception exception)
    {
        if (exception is not null)
        {
            return LogEventLevel.Error;
        }

        return context.Response.StatusCode switch
        {
            <= 100 => LogEventLevel.Warning,
            (>= 200) and (<= 399) => LogEventLevel.Information,
            (>= 100) and (>= 400) and (<= 499) => LogEventLevel.Warning,
            _ => LogEventLevel.Error,
        };
    }
}
