using CatalogsManagementTool.Core.Logging;
using Microsoft.AspNetCore.HttpLogging;

namespace CatalogsManagementTool.Core.Settings.LoggingSettings;

public static class HttpLoggingSettings
{
    public static HttpLoggingOptions Configure(HttpLoggingOptions options)
    {
        options.LoggingFields = HttpLoggingFields.All;
        options.RequestHeaders.Add(LoggingConsts.CorrelationId);

        return options;
    }
}
