using CatalogsManagementTool.Core.Logging;
using CorrelationId;

namespace CatalogsManagementTool.Core.Settings.LoggingSettings;

public static class CorrelationIdSettings
{
    public static CorrelationIdOptions Configure(CorrelationIdOptions options)
    {
        options.RequestHeader = LoggingConsts.CorrelationId;
        options.ResponseHeader = LoggingConsts.CorrelationId;

        options.IncludeInResponse = true;
        options.AddToLoggingScope = true;

        return options;
    }
}
