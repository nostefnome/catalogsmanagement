using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace CatalogsManagementTool.Core.Settings;

public static class GlobalConfigurationSettings
{
    private const string AppSettingsFile = "appsettings";

    public static IConfigurationBuilder Configure(HostBuilderContext context, IConfigurationBuilder builder)
    {
        builder.AddEnvironmentVariables();

        var sharedFolder = Path.Combine(
            GetRoot(),
            builder.Build()["GlobalConfiguration"]);

        var appSettigns = $"{AppSettingsFile}.json";
        var envAppSettings = BuildEnvironmentSettingsFileName(AppSettingsFile, context);

        var commonSettings = Path.Combine(sharedFolder, $"{AppSettingsFile}.json");
        var commonEnvSettings = Path.Combine(sharedFolder, BuildEnvironmentSettingsFileName(AppSettingsFile, context));

        return builder
            .AddJsonFile(commonSettings, false, true)
            .AddJsonFile(appSettigns, false, true)
            .AddJsonFile(commonEnvSettings, false, true)
            .AddJsonFile(envAppSettings, false, true);
    }

    private static string GetRoot()
    {
        return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
    }

    private static string BuildEnvironmentSettingsFileName(string fileName, HostBuilderContext context)
    {
        return $"{fileName}.{context.HostingEnvironment.EnvironmentName}.json";
    }
}
