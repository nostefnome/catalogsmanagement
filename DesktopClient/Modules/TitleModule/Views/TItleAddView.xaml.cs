﻿using Modules.Infrastructure.Regions;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace TitleModule.Views;

public partial class TitleAddView : UserControl
{
    private readonly IContainerExtension _container;

    public TitleAddView(IContainerExtension container)
    {
        InitializeComponent();

        _container = container;
        Loaded += TitleAddView_Loaded;
    }

    private void TitleAddView_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        var regionManager = RegionManager.GetRegionManager(this);
        var titleInputView = _container.Resolve<TitleInputView>();

        regionManager.Regions[TitleRegions.TitleInput]
            .Add(titleInputView, null, true);
    }
}