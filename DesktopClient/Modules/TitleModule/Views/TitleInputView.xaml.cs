﻿using Modules.Infrastructure.Models;
using Prism.Common;
using Prism.Regions;
using System.Windows.Controls;
using TitleModule.ViewModels;

namespace TitleModule.Views;

public partial class TitleInputView : UserControl
{
    public TitleInputView()
    {
        InitializeComponent();
        RegionContext.GetObservableContext(this).PropertyChanged += InputTitleView_PropertyChanged;
    }

    private void InputTitleView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        var context = (ObservableObject<object>)sender;
        (DataContext as TitleInputViewModel).Title = (TitleModel)context.Value;
    }
}
