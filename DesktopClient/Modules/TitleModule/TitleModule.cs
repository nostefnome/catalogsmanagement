﻿using Prism.Ioc;
using Prism.Modularity;
using TitleModule.Views;

namespace TitleModule;

public class TitleModule : IModule
{
    public void OnInitialized(IContainerProvider containerProvider) { }

    public void RegisterTypes(IContainerRegistry containerRegistry)
    {
        containerRegistry.RegisterForNavigation<TitlesView>();
        containerRegistry.RegisterForNavigation<TitleAddView>();
        containerRegistry.RegisterForNavigation<TitleUpdateView>();
    }
}