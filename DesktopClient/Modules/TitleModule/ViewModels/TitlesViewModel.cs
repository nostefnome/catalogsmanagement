﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract;
using Modules.Infrastructure.Models;
using Modules.Infrastructure.Regions;
using Modules.Infrastructure.ViewModels;
using MvvmDialogs.Prism.DialogService;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TitleModule.Views;

namespace TitleModule.ViewModels;

public class TitlesViewParameters
{
    public CatalogModel Catalog { get; set; }
}

public class TitlesViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IRegionManager _regionManager;
    private readonly IDialogService _dialogService;
    private readonly IMapper _mapper;

    private IRegionNavigationJournal _journal;

    private ObservableCollection<TitleModel> _titles;
    public ObservableCollection<TitleModel> Titles
    {
        get { return _titles; }
        set { SetProperty(ref _titles, value); }
    }

    private CatalogModel _catalog;
    public CatalogModel Catalog
    {
        get { return _catalog; }
        set { SetProperty(ref _catalog, value); }
    }

    public DelegateCommand AddTitleCommand { get; private set; }
    public DelegateCommand GoBackCommand { get; private set; }

    public TitlesViewModel(
        ICatalogClient catalogClient, 
        IRegionManager regionManager,
        IDialogService dialogService,
        IMapper mapper)
    {
        _catalogClient = catalogClient;
        _regionManager = regionManager;
        _dialogService = dialogService;
        _mapper = mapper;

        AddTitleCommand = new DelegateCommand(AddTitleCommandExecute, CanAddTitleCommandExecute)
            .ObservesProperty(() => Catalog);
        GoBackCommand = new DelegateCommand(GoBackCommandExecute);
    }

    private void GoBackCommandExecute()
    {
        _journal.GoBack();
    }

    private bool CanAddTitleCommandExecute()
    {
        return Catalog != null;
    }

    public void OnUpdateTitle(TitleModel title)
    {
        var parameters = new NavigationParameters();
        parameters.Add(nameof(TitleUpdateViewParameters), new TitleUpdateViewParameters
        {
            Title = title,
        });

        _regionManager.RequestNavigate(CatalogRegions.Content, nameof(TitleUpdateView), parameters);
    }

    public async void OnNavigatedTo(NavigationContext navigationContext) 
    {
        var parameters = (TitlesViewParameters)navigationContext.Parameters[nameof(TitlesViewParameters)];
        var catalog = parameters?.Catalog;
        var titles = await GetTitles(catalog);

        _journal = navigationContext.NavigationService.Journal;

        Catalog = catalog;
        Titles = new ObservableCollection<TitleModel>(titles);
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext) { }

    private void AddTitleCommandExecute()
    {
        var parameters = new NavigationParameters();
        parameters.Add(nameof(TitleAddNavigationParameters), new TitleAddNavigationParameters
        {
            CatalogId = Catalog.CatalogId
        });

        _regionManager.RequestNavigate(CatalogRegions.Content, nameof(TitleAddView), parameters);
    }

    private async Task<IEnumerable<TitleModel>> GetTitles(CatalogModel catalog)
    {
        try
        {
            return (await _catalogClient.GetTitles(catalog.Name))
                .Select(title => _mapper.Map<TitleModel>(title));    
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while loading titles: {e.Message}";
            });
        }

        return Array.Empty<TitleModel>();
    }
}
