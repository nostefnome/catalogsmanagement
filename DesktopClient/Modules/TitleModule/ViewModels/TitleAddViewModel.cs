﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Requests;
using Modules.Infrastructure.Models;
using Modules.Infrastructure.ViewModels;
using MvvmDialogs.Prism.DialogService;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Threading.Tasks;

namespace TitleModule.ViewModels;

public class TitleAddNavigationParameters
{
    public Guid CatalogId { get; set; }
}

[RegionMemberLifetime(KeepAlive = false)]
public class TitleAddViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IDialogService _dialogService;
    private IRegionNavigationJournal _journal;

    private Guid _catalogId;

    private TitleModel _title = new();
    public TitleModel Title
    {
        get { return _title; }
        set { SetProperty(ref _title, value); }
    }

    public DelegateCommand AddTitleCommand { get; private set; }
    public DelegateCommand GoBackCommand { get; private set; }

    public TitleAddViewModel(ICatalogClient catalogClient, IDialogService dialogService)
    {
        _catalogClient = catalogClient;
        _dialogService = dialogService;

        AddTitleCommand = new DelegateCommand(
            async () => await AddTitleCommandExecute(), 
            CanExecuteAddTitleCommand)
                .ObservesProperty(() => Title.Name)
                .ObservesProperty(() => Title.Tag)
                .ObservesProperty(() => Title.Count);
        GoBackCommand = new DelegateCommand(GoBackCommandExecute);
    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
        var parameters = (TitleAddNavigationParameters)navigationContext.Parameters[nameof(TitleAddNavigationParameters)];

        _catalogId = parameters.CatalogId;
        _journal = navigationContext.NavigationService.Journal;
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext) { }

    private void GoBackCommandExecute()
    {
        _journal.GoBack();
    }

    private bool CanExecuteAddTitleCommand()
    {
        return
            !string.IsNullOrWhiteSpace(Title.Name) &&
            !string.IsNullOrWhiteSpace(Title.Tag) &&
            Title.Count > 0;
    }

    private async Task AddTitleCommandExecute()
    {
        var confirm = _dialogService.ShowDialog<ConfirmationDialogViewModel>(this, context =>
        {
            context.Title = "Confirmation";
            context.Message = $"Add title {Title.Name}?";
        });

        if (confirm != true)
        {
            return;
        }

        try
        {
            await _catalogClient.AddTitle(new AddTitleRequest
            {
                CatalogId = _catalogId,
                Name = Title.Name,
                Tag = Title.Tag,
                Count = Title.Count
            });
            
            Title = new TitleModel
            {
                Count = 1
            };
        }
        catch (Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while adding title: {e.Message}";
            });
        }
        
    }
}
