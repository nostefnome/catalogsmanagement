﻿using Modules.Infrastructure.Models;
using Prism.Mvvm;

namespace TitleModule.ViewModels;

public class TitleInputViewModel : BindableBase
{
    private TitleModel _title;
    public TitleModel Title
    {
        get { return _title; }
        set { SetProperty(ref _title, value); }
    }

    public TitleInputViewModel() { }
}
