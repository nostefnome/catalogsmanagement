﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Requests;
using Modules.Infrastructure.Models;
using Modules.Infrastructure.ViewModels;
using MvvmDialogs.Prism.DialogService;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;

namespace TitleModule.ViewModels;

public class TitleUpdateViewParameters
{
    public TitleModel Title { get; set; }
}

[RegionMemberLifetime(KeepAlive = false)]
public class TitleUpdateViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IDialogService _dialogService;
    private IRegionNavigationJournal _journal;

    private TitleModel _title;
    public TitleModel Title
    {
        get { return _title; }
        set { SetProperty(ref _title, value); }
    }

    private string _titleName;
    public string TitleName
    {
        get { return _titleName; }
        set { SetProperty(ref _titleName, value); }
    }

    public DelegateCommand UpdateTitleCommand { get; private set; }
    public DelegateCommand RemoveTitleCommand { get; private set; }
    public DelegateCommand GoBackCommand { get; private set; }

    public TitleUpdateViewModel(ICatalogClient catalogClient, IDialogService dialogService)
    {
        _catalogClient = catalogClient;
        _dialogService = dialogService;

        UpdateTitleCommand = new DelegateCommand(UpdateTitleCommandExecute, CanUpdateTitleCommand)
            .ObservesProperty(() => Title.Name)
            .ObservesProperty(() => Title.Tag)
            .ObservesProperty(() => Title.Count);
        RemoveTitleCommand = new DelegateCommand(RemoveTitleCommandExecute);
        GoBackCommand = new DelegateCommand(GoBackCommandExecute);
    }

    private async void RemoveTitleCommandExecute()
    {
        var confirm = _dialogService.ShowDialog<ConfirmationDialogViewModel>(this, context =>
        {
            context.Title = "Confirmation";
            context.Message = $"Remove title {Title.Name}?";
        });

        if (confirm != true)
        {
            return;
        }

        try
        {
            await _catalogClient.RemoveTitle(Title.TitleId);
            _journal.GoBack();
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while removing title: {e.Message}";
            });
        }
    }

    private void GoBackCommandExecute()
    {
        _journal.GoBack();
    }

    private bool CanUpdateTitleCommand()
    {
        return 
            !string.IsNullOrWhiteSpace(Title?.Name) && 
            !string.IsNullOrWhiteSpace(Title?.Tag) &&
            Title.Count > 0;
    }

    private async void UpdateTitleCommandExecute()
    {
        var confirm = _dialogService.ShowDialog<ConfirmationDialogViewModel>(this, context =>
        {
            context.Title = "Confirmation";
            context.Message = $"Update title {Title.Name}?";
        });

        if (confirm != true)
        {
            return;
        }

        try
        {
            await _catalogClient.UpdateTitle(new UpdateTitleRequest
            {
                CatalogId = Title.CatalogId,
                TitleId = Title.TitleId,
                Name = Title.Name,
                Tag = Title.Tag,
                Count = Title.Count,
            });
            _journal.GoBack();
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while updating title: {e.Message}";
            });
        }
    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
        var parameters = navigationContext.Parameters[nameof(TitleUpdateViewParameters)] as TitleUpdateViewParameters;
        var title = parameters?.Title;

        Title = title;
        TitleName = title.Name;
        _journal = navigationContext.NavigationService.Journal;
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext) { }
}
