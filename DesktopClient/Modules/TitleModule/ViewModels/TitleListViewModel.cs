﻿using Modules.Infrastructure.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;

namespace TitleModule.ViewModels;

public class TitleListViewModel : BindableBase
{
    private ObservableCollection<TitleModel> _titles;
    public ObservableCollection<TitleModel> Titles
    {
        get { return _titles; }
        set { SetProperty(ref _titles, value); }
    }

    public event Action<TitleModel> UpdateTitleEvent;
    public DelegateCommand<TitleModel> UpdateTitleCommand { get; private set; }

    public TitleListViewModel()
    {
        UpdateTitleCommand = new DelegateCommand<TitleModel>(title =>
            UpdateTitleEvent?.Invoke(title));
    }
}
