﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.ObjectModel;

namespace TitleModule.ViewModels;

public class TitlesViewParameters
{
    public CatalogModel catalogModel { get; set; }
}
//TODO: Load Titles from backend
//TODO: Set in xaml title as "Catalog {Catalog.Name}"
public class TitlesViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IRegionManager _regionManager;

    private ObservableCollection<Models.TitleModel> _titles;
    public ObservableCollection<Models.TitleModel> Titles
    {
        get { return _titles; }
        set { SetProperty(ref _titles, value); }
    }

    private CatalogModel _catalogModel;
    public CatalogModel CatalogModel
    {
        get { return _catalogModel; }
        set { SetProperty(ref _catalogModel, value); }
    }

    public DelegateCommand AddTitleCommand { get; private set; }

    public TitlesViewModel(ICatalogClient catalogClient, IRegionManager regionManager)
    {
        _catalogClient = catalogClient;
        _regionManager = regionManager;

        AddTitleCommand = new DelegateCommand(AddTitleCommandExecute);
    }

    private void AddTitleCommandExecute()
    {
        
    }

    public void OnUpdateTitle(Models.TitleModel titleModel)
    {

    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
        var parameters = navigationContext.Parameters[nameof(TitlesViewParameters)] as TitlesViewParameters;

        CatalogModel = parameters?.catalogModel;
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext)
    {
        
    }
}
