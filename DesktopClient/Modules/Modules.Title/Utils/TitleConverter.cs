﻿using System;
using System.Globalization;
using System.Windows.Data;
using TitleModule.Models;

namespace TitleModule.Utils;

public class TitleConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var title = (TitleModel)value;
        return $"{title.Name} - ${title.Tag} ({title.Count})";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}
