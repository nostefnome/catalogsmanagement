﻿using Prism.Mvvm;
using System;

namespace TitleModule.Models;

public class TitleModel : BindableBase
{
    public Guid TitleId { get; set; }
    public Guid CatalogId { get; set; }

    private string _name;
    public string Name
    {
        get { return _name; }
        set { SetProperty(ref _name, value); }
    }
    private string _tag;
    public string Tag
    {
        get { return _tag; }
        set { SetProperty(ref _tag, value); }
    }
    private int _count;
    public int Count
    {
        get { return _count; }
        set { SetProperty(ref _count, value); }
    }
}
