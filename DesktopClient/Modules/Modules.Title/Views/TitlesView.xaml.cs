﻿using Modules.Infrastructure.Regions;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;
using TitleModule.ViewModels;

namespace TitleModule.Views;

public partial class TitlesView : UserControl
{
    private readonly IContainerExtension _container;

    public TitlesView(IContainerExtension container)
    {
        _container = container;
        Loaded += TitlesView_Loaded;
    }

    private void TitlesView_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        var context = DataContext as TitlesViewModel;
        var regionManager = RegionManager.GetRegionManager(this);

        var titlesList = _container.Resolve<TitleListView>();
        titlesList.Context.UpdateTitleEvent += context.OnUpdateTitle;

        //regionManager.Regions[TitleRegions.TitleList]
        //    .Add(titlesList, null, true);
    }
}