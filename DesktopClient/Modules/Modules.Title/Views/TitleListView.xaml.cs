﻿using Prism.Common;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using TitleModule.Models;
using TitleModule.ViewModels;

namespace TitleModule.Views;

public partial class TitleListView : UserControl
{
    public TitleListViewModel Context
    {
        get { return DataContext as TitleListViewModel; }
    }

    public TitleListView()
    {
        InitializeComponent();
        RegionContext.GetObservableContext(this).PropertyChanged += TitleList_PropertyChanged;
    }

    private void TitleList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        var context = (ObservableObject<object>)sender;
        (DataContext as TitleListViewModel).Titles = (ObservableCollection<TitleModel>)context.Value;
    }
}
