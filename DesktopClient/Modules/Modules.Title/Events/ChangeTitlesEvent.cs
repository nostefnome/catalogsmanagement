﻿using Prism.Events;
using System.Collections.Generic;
using TitleModule.Models;

namespace TitleModule.Events;

public class ChangeTitlesEvent : PubSubEvent<IEnumerable<TitleModel>> { }
