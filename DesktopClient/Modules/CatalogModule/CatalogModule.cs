﻿using CatalogModule.Views;
using Modules.Infrastructure.Regions;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace CatalogModule;

public class CatalogModule : IModule
{
    public void OnInitialized(IContainerProvider containerProvider)
    {
        var regionManager = containerProvider.Resolve<IRegionManager>();
        regionManager.RequestNavigate(CatalogRegions.Content, nameof(CatalogsView));
    }

    public void RegisterTypes(IContainerRegistry containerRegistry)
    {
        containerRegistry.RegisterForNavigation<CatalogsView>();
        containerRegistry.RegisterForNavigation<CatalogAddView>();
        containerRegistry.RegisterForNavigation<CatalogUpdateView>();
    }
}