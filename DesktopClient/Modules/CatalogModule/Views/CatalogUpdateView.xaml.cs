﻿using Modules.Infrastructure.Regions;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace CatalogModule.Views;

public partial class CatalogUpdateView : UserControl
{
    private readonly IContainerExtension _container;

    public CatalogUpdateView(IContainerExtension container)
    {
        InitializeComponent();

        _container = container;
        Loaded += CatalogUpdateView_Loaded;
    }

    private void CatalogUpdateView_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        var regionManager = RegionManager.GetRegionManager(this);
        var catalogInputView = _container.Resolve<CatalogInputView>();

        regionManager.Regions[CatalogRegions.InputCatalog]
            .Add(catalogInputView, null, true);
    }
}
