﻿
using CatalogModule.ViewModels;
using Modules.Infrastructure.Regions;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace CatalogModule.Views;

public partial class CatalogsView : UserControl
{
    private readonly IContainerExtension _container;

    public CatalogsView(IContainerExtension container)
    {
        InitializeComponent();

        _container = container;
        Loaded += CatalogsView_Loaded;
    }

    private void CatalogsView_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        var context = DataContext as CatalogsViewModel;
        var regionManager = RegionManager.GetRegionManager(this);
        
        var catalogList = _container.Resolve<CatalogListView>();
        catalogList.Context.UpdateCatalogEvent += context.OnUpdateCatalog;
        catalogList.Context.ViewCatalogEvent += context.OnViewCatalog;

        regionManager.Regions[CatalogRegions.CatalogList]
            .Add(catalogList, null, true);
    }
}
