﻿using CatalogModule.ViewModels;
using Modules.Infrastructure.Models;
using Prism.Common;
using Prism.Regions;
using System.Windows.Controls;

namespace CatalogModule.Views;

public partial class CatalogInputView : UserControl
{
    public CatalogInputView()
    {
        InitializeComponent();
        RegionContext.GetObservableContext(this).PropertyChanged += CatalogInputView_PropertyChanged;
    }

    private void CatalogInputView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        var context = (ObservableObject<object>)sender;
        (DataContext as CatalogInputViewModel).Catalog = (CatalogModel)context.Value;
    }
}
