﻿using CatalogModule.ViewModels;
using Modules.Infrastructure.Models;
using Prism.Common;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace CatalogModule.Views;

public partial class CatalogListView : UserControl
{
    public CatalogListViewModel Context
    {
        get { return DataContext as CatalogListViewModel; }
    }

    public CatalogListView()
    {
        InitializeComponent();
        RegionContext.GetObservableContext(this).PropertyChanged += CatalogList_PropertyChanged;
    }

    private void CatalogList_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
    {
        var context = (ObservableObject<object>)sender;
        (DataContext as CatalogListViewModel).Catalogs = (ObservableCollection<CatalogModel>)context.Value;
    }
}
