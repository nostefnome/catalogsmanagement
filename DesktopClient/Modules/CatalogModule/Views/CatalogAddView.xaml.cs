﻿using Modules.Infrastructure.Regions;
using Prism.Ioc;
using Prism.Regions;
using System.Windows.Controls;

namespace CatalogModule.Views;

public partial class CatalogAddView : UserControl
{
    private readonly IContainerExtension _container;
    
    public CatalogAddView(IContainerExtension container)
    {
        InitializeComponent();

        _container = container;
        Loaded += AddCatalogView_Loaded;
    }

    private void AddCatalogView_Loaded(object sender, System.Windows.RoutedEventArgs e)
    {
        var regionManager = RegionManager.GetRegionManager(this);
        var catalogInputView = _container.Resolve<CatalogInputView>();

        regionManager.Regions[CatalogRegions.InputCatalog]
            .Add(catalogInputView, null, true);
    }
}
