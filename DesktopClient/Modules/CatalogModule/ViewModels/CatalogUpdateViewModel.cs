﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Requests;
using Modules.Infrastructure.Models;
using Modules.Infrastructure.ViewModels;
using MvvmDialogs.Prism.DialogService;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;

namespace CatalogModule.ViewModels;

public class CatalogUpdateNavigationParameters
{
    public CatalogModel Catalog { get; set; }
}

[RegionMemberLifetime(KeepAlive = false)]
public class CatalogUpdateViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IDialogService _dialogService;
    private IRegionNavigationJournal _journal;

    private CatalogModel _catalog = new();
    public CatalogModel Catalog
    {
        get { return _catalog; }
        set { SetProperty(ref _catalog, value); }
    }

    private string _catalogName;
    public string CatalogName
    {
        get { return _catalogName; }
        set { SetProperty(ref _catalogName, value); }
    }

    public DelegateCommand UpdateCatalogCommand { get; private set; }
    public DelegateCommand RemoveCatalogCommand { get; private set; }
    public DelegateCommand GoBackCommand { get; private set; }

    public CatalogUpdateViewModel(ICatalogClient catalogClient, IDialogService dialogService)
    {
        _catalogClient = catalogClient;
        _dialogService = dialogService;

        UpdateCatalogCommand = new DelegateCommand(UpdateCatalogCommandExecute, CanUpdateCatalogCommand)
            .ObservesProperty(() => Catalog.Name);
        RemoveCatalogCommand = new DelegateCommand(RemoveCatalogCommandExecute);
        GoBackCommand = new DelegateCommand(GoBackCommandExecute);
    }

    private async void RemoveCatalogCommandExecute()
    {
        var confirm = _dialogService.ShowDialog<ConfirmationDialogViewModel>(this, context =>
        {
            context.Title = "Confirmation";
            context.Message = $"Remove catalog {Catalog.Name}?";
        });

        if (confirm != true)
        {
            return;
        }

        try
        {
            await _catalogClient.RemoveCatalog(Catalog.CatalogId);
            _journal.GoBack();
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while removing catalogs: {e.Message}";
            });
        }
    }

    private void GoBackCommandExecute()
    {
        _journal.GoBack();
    }

    private bool CanUpdateCatalogCommand()
    {
        return !string.IsNullOrWhiteSpace(Catalog?.Name);
    }

    private async void UpdateCatalogCommandExecute()
    {
        var confirm = _dialogService.ShowDialog<ConfirmationDialogViewModel>(this, context =>
        {
            context.Title = "Confirmation";
            context.Message = $"Update catalog {Catalog.Name}?";
        });

        if (confirm != true)
        {
            return;
        }

        try
        {
            await _catalogClient.UpdateCatalog(new UpdateCatalogRequest
            {
                CatalogId = Catalog.CatalogId,
                Name = Catalog.Name,
            });
            _journal.GoBack();
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while updating catalog: {e.Message}";
            });
        }
    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
        var parameters = (CatalogUpdateNavigationParameters)navigationContext.Parameters[nameof(CatalogUpdateNavigationParameters)];
        var catalog = parameters?.Catalog;

        Catalog = catalog;
        CatalogName = catalog?.Name;
        _journal = navigationContext.NavigationService.Journal;
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext) { }
}
