﻿using CatalogsManagementTool.Catalog.Contract;
using CatalogsManagementTool.Catalog.Contract.Requests;
using Modules.Infrastructure.Models;
using Modules.Infrastructure.ViewModels;
using MvvmDialogs.Prism.DialogService;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Threading.Tasks;

namespace CatalogModule.ViewModels;

[RegionMemberLifetime(KeepAlive = false)]
public class CatalogAddViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IDialogService _dialogService;
    private IRegionNavigationJournal _journal;

    private CatalogModel _catalog = new();
    public CatalogModel Catalog
    {
        get { return _catalog; }
        set { SetProperty(ref _catalog, value); }
    }

    public DelegateCommand AddCatalogCommand { get; private set; }
    public DelegateCommand GoBackCommand { get; private set; }

    public CatalogAddViewModel(ICatalogClient catalogClient, IDialogService dialogService)
    {
        _catalogClient = catalogClient;
        _dialogService = dialogService;

        AddCatalogCommand = new DelegateCommand(async () =>  await 
            AddCatalogCommandExecute(), 
            CanExecuteAddCatalogCommand)
                .ObservesProperty(() => Catalog.Name);

        GoBackCommand = new DelegateCommand(GoBackCommandExecute);
    }

    private bool CanExecuteAddCatalogCommand()
    {
        return !string.IsNullOrWhiteSpace(Catalog.Name);
    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
        _journal = navigationContext.NavigationService.Journal;
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext) { }

    private async Task AddCatalogCommandExecute()
    {
        var confirm = _dialogService.ShowDialog<ConfirmationDialogViewModel>(this, context =>
        {
            context.Title = "Confirmation";
            context.Message = $"Add new catalog {Catalog.Name}?";
        });

        if (confirm != true)
        {
            return;
        }

        try
        {
            await _catalogClient.AddCatalog(new AddCatalogRequest
            {
                Name = Catalog.Name
            });

            Catalog = new CatalogModel();
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Error while adding catalog: {e.Message}";
            });
        }
    }

    private void GoBackCommandExecute()
    {
        _journal.GoBack();
    }
}