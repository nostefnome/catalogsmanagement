﻿using Modules.Infrastructure.Models;
using Prism.Mvvm;

namespace CatalogModule.ViewModels;

public class CatalogInputViewModel : BindableBase
{
    private CatalogModel _catalog;
    public CatalogModel Catalog
    {
        get { return _catalog; }
        set { SetProperty(ref _catalog, value); }
    }

    public CatalogInputViewModel() { }
}
