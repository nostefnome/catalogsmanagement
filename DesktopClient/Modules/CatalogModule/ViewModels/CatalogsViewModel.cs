﻿using AutoMapper;
using CatalogModule.Views;
using CatalogsManagementTool.Catalog.Contract;
using Modules.Infrastructure.Models;
using Modules.Infrastructure.Regions;
using Modules.Infrastructure.ViewModels;
using MvvmDialogs.Prism.DialogService;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using TitleModule.ViewModels;
using TitleModule.Views;

namespace CatalogModule.ViewModels;

public class CatalogsViewModel : BindableBase, INavigationAware
{
    private readonly ICatalogClient _catalogClient;
    private readonly IRegionManager _regionManager;
    private readonly IDialogService _dialogService;
    private readonly IMapper _mapper;

    private IRegionNavigationJournal _journal;

    private ObservableCollection<CatalogModel> _catalogs;
    public ObservableCollection<CatalogModel> Catalogs
    {
        get { return _catalogs; }
        set { SetProperty(ref _catalogs, value); }
    }

    public DelegateCommand LoadCatalogsCommand { get; private set; }
    public DelegateCommand AddCatalogCommand { get; private set; }

    public CatalogsViewModel(
        ICatalogClient catalogClient,
        IRegionManager regionManager,
        IDialogService dialogService,
        IMapper mapper)
    {
        _catalogClient = catalogClient;
        _regionManager = regionManager;
        _dialogService = dialogService;
        _mapper = mapper;

        LoadCatalogsCommand = new DelegateCommand(LoadCatalogs);
        AddCatalogCommand = new DelegateCommand(AddCatalogCommandExecute);
    }

    public void OnNavigatedTo(NavigationContext navigationContext)
    {
        _journal = navigationContext.NavigationService.Journal;
        LoadCatalogs();
    }

    public bool IsNavigationTarget(NavigationContext navigationContext)
    {
        return true;
    }

    public void OnNavigatedFrom(NavigationContext navigationContext) { }

    public void OnViewCatalog(CatalogModel catalog)
    {
        var parameters = new NavigationParameters();
        parameters.Add(nameof(TitlesViewParameters), new TitlesViewParameters
        {
            Catalog = catalog
        });

        _regionManager.RequestNavigate(CatalogRegions.Content, nameof(TitlesView), parameters);
    }

    public void OnUpdateCatalog(CatalogModel catalog)
    {
        var parameters = new NavigationParameters();
        parameters.Add(nameof(CatalogUpdateNavigationParameters), new CatalogUpdateNavigationParameters
        {
            Catalog = catalog
        });

        _regionManager.RequestNavigate(CatalogRegions.Content, nameof(CatalogUpdateView), parameters);
    }

    private void AddCatalogCommandExecute()
    {
        _regionManager.RequestNavigate(CatalogRegions.Content, nameof(CatalogAddView));
    }

    private async void LoadCatalogs()
    {
        try
        {
            var catalogs = (await _catalogClient.GetAllCatalogs())
                .Select(x => _mapper.Map<CatalogModel>(x));
            Catalogs = new ObservableCollection<CatalogModel>(catalogs);
        }
        catch(Exception e)
        {
            _dialogService.ShowDialog<NotificationDialogViewModel>(this, context =>
            {
                context.Title = "Error";
                context.Message = $"Exception while loading catalogs: {e.Message}";
            });
        }
    }
}