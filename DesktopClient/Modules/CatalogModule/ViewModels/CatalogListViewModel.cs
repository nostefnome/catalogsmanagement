﻿using Modules.Infrastructure.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;

namespace CatalogModule.ViewModels;

public class CatalogListViewModel : BindableBase
{
    private ObservableCollection<CatalogModel> _catalogs;
    public ObservableCollection<CatalogModel> Catalogs
    {
        get { return _catalogs; }
        set { SetProperty(ref _catalogs, value); }
    }

    public event Action<CatalogModel> UpdateCatalogEvent;
    public event Action<CatalogModel> ViewCatalogEvent;

    public DelegateCommand<CatalogModel> UpdateCatalogCommand { get; private set; }
    public DelegateCommand<CatalogModel> ViewCatalogCommand { get; private set; }

    public CatalogListViewModel() 
    {
        UpdateCatalogCommand = new DelegateCommand<CatalogModel>(catalog =>
            UpdateCatalogEvent?.Invoke(catalog));
        ViewCatalogCommand = new DelegateCommand<CatalogModel>(catalog =>
            ViewCatalogEvent?.Invoke(catalog));
    }
}
