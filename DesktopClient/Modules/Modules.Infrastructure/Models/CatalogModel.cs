﻿using Prism.Mvvm;
using System;

namespace Modules.Infrastructure.Models;

public class CatalogModel : BindableBase
{
    private Guid _catalogId;
    public Guid CatalogId
    {
        get { return _catalogId; }
        set { SetProperty(ref _catalogId, value); }
    }

    private string _name;
    public string Name
    {
        get { return _name; }
        set { SetProperty(ref _name, value); }
    }
}
