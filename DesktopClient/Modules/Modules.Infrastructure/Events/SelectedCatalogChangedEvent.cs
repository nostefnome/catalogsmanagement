﻿using Modules.Infrastructure.Models;
using Prism.Events;

namespace Modules.Infrastructure.Events;

public class SelectedCatalogChangedEvent : PubSubEvent<CatalogModel> { }
