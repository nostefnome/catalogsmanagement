﻿using Modules.Infrastructure.Models;
using Prism.Events;
using System.Collections.Generic;

namespace Modules.Infrastructure.Events;

public class ChangeTitlesEvent : PubSubEvent<IEnumerable<TitleModel>> { }
