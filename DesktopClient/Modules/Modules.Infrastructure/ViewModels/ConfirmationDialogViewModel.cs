﻿using MvvmDialogs;
using Prism.Commands;
using Prism.Mvvm;

namespace Modules.Infrastructure.ViewModels;

public class ConfirmationDialogViewModel : BindableBase, IModalDialogViewModel
{
    private bool? _dialogResult;
    public bool? DialogResult
    {
        get { return _dialogResult; }
        set { SetProperty(ref _dialogResult, value); }
    }

    private string _title;
    public string Title
    {
        get { return _title; }
        set { SetProperty(ref _title, value); }
    }

    private string _message;
    public string Message
    {
        get { return _message; }
        set { SetProperty(ref _message, value); }
    }

    public DelegateCommand ConfirmCommand { get; set; }

    public ConfirmationDialogViewModel()
    {
        ConfirmCommand = new DelegateCommand(ConfirmCommandExecute);
    }

    private void ConfirmCommandExecute()
    {
        DialogResult = true;
    }
}