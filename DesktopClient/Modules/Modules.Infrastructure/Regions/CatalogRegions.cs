﻿namespace Modules.Infrastructure.Regions;

public static class CatalogRegions
{
    public const string Content = "CatalogContentRegion";

    public const string CatalogList = "CatalogListRegion";
    public const string InputCatalog = "InputCatalogRegion";
}
