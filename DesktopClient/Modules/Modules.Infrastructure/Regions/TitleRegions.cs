﻿namespace Modules.Infrastructure.Regions;

public static class TitleRegions
{
    public const string Content = "TitleContentRegion";
    public const string TitleList = "TitleListRegion";
    public const string TitleInput = "TitleInputRegion";
}
