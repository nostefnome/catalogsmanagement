﻿using System.ComponentModel;

namespace MvvmDialogs.Prism.DialogService;

public interface IDialogService
{
    bool? ShowDialog<TdialogViewModel>(
        INotifyPropertyChanged owner,
        Action<TdialogViewModel> beforeExecuteCallback = null)
            where TdialogViewModel : IModalDialogViewModel;
}