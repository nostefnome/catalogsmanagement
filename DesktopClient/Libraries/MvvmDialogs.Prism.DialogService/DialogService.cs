﻿using Prism.Ioc;
using System.ComponentModel;

namespace MvvmDialogs.Prism.DialogService;

public class DialogService : IDialogService
{
    private readonly IContainerExtension _container;
    private readonly MvvmDialogs.IDialogService _dialogService;

    public DialogService(MvvmDialogs.IDialogService dialogService, IContainerExtension container)
    {
        _dialogService = dialogService;
        _container = container;
    }

    public bool? ShowDialog<TdialogViewModel>(
        INotifyPropertyChanged owner,
        Action<TdialogViewModel> beforeExecuteCallback = null)
            where TdialogViewModel : IModalDialogViewModel
    {
        var dialogViewModel = _container.Resolve<TdialogViewModel>();

        beforeExecuteCallback?.Invoke(dialogViewModel);
        _dialogService.ShowDialog(owner, dialogViewModel);

        return dialogViewModel.DialogResult;
    }
}
