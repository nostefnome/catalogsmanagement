﻿using MvvmDialogs.DialogFactories;
using MvvmDialogs.DialogTypeLocators;
using Prism.Ioc;

namespace MvvmDialogs.Prism.DialogService;

public static class IocContainerExtension
{
    public static IContainerRegistry UseMvvmPrismDialogService(this IContainerRegistry containerRegistry)
    {
        containerRegistry.Register<IDialogService, DialogService>();
        containerRegistry.Register<MvvmDialogs.IDialogService, MvvmDialogs.DialogService>();
        containerRegistry.Register<IDialogTypeLocator, DialogTypeLocator>();
        containerRegistry.Register<IDialogFactory, DialogFactory>();

        return containerRegistry;
    }
}
