﻿using MvvmDialogs.DialogFactories;
using Prism.Ioc;
using System.Windows;

namespace MvvmDialogs.Prism.DialogService;

internal class DialogFactory : IDialogFactory
{
    private readonly IContainerExtension _container;

    public DialogFactory(IContainerExtension container)
    {
        _container = container;
    }

    public IWindow Create(Type dialogType)
    {
        if (dialogType == null) throw new ArgumentNullException(nameof(dialogType));

        var instance = _container.Resolve(dialogType);

        return instance switch
        {
            IWindow customDialog => customDialog,
            Window dialog => new WindowWrapper(dialog),
            _ => throw new ArgumentException($"Only dialogs of type {typeof(Window)} or {typeof(IWindow)} are supported.")
        };
    }
}
