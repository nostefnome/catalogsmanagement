﻿using MvvmDialogs.DialogTypeLocators;
using System.ComponentModel;

namespace MvvmDialogs.Prism.DialogService;

public class DialogTypeLocator : IDialogTypeLocator
{
    public Type Locate(INotifyPropertyChanged viewModel)
    {
        var viewModelType = viewModel.GetType();

        var viewModelName = viewModelType.Name;
        var assemblyName = viewModelType.Assembly.FullName.Split(",")[0];

        var dialogTypeName = viewModelName
            .Substring(0, viewModelName.Length - "ViewModel".Length);
        var fullName = $"{assemblyName}.Views.{dialogTypeName}View";
        var dialog = viewModelType.Assembly.GetType(fullName);

        return dialog;
    }
}
