﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Reflection;

namespace DesktopClient.Utils;

internal static class ConfigurationUtility
{
    private const string AppSettingsFile = "appsettings";
    private const string ConfigurationFolder = "Configuration";

    public static IConfigurationBuilder Configure(IConfigurationBuilder builder)
    {
        var sharedFolder = Path.Combine(GetRoot(), ConfigurationFolder);
        var environmentName = Environment.GetEnvironmentVariable("EnvironmentName");

        var appsettings = Path.Combine(sharedFolder, BuildFileName(AppSettingsFile));
        var envAppsettings = Path.Combine(sharedFolder, BuildFileName(AppSettingsFile, environmentName));

        return builder
            .AddJsonFile(appsettings, false, true)
            .AddJsonFile(envAppsettings, false, true);
    }

    private static string GetRoot()
    {
        return Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
    }

    private static string BuildFileName(string fileName, string envName = null)
    {
        return envName == null ?
            $"{fileName}.json" :
            $"{fileName}.{envName}.json";
    }
}
