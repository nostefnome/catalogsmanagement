﻿using AutoMapper;
using Modules.Infrastructure.Models;

namespace DesktopClient.Utils;

internal class AutoMapperProfile : Profile
{
    public AutoMapperProfile()
    {
        CreateMap<CatalogsManagementTool.Catalog.Contract.Models.CatalogModel, CatalogModel>();
        CreateMap<CatalogsManagementTool.Catalog.Contract.Models.TitleModel, TitleModel>();
    }
}
