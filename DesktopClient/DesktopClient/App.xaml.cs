﻿using AutoMapper;
using CatalogsManagementTool.Catalog.Contract;
using DesktopClient.Utils;
using DesktopClient.Views;
using Microsoft.Extensions.Configuration;
using MvvmDialogs.Prism.DialogService;
using Prism.Ioc;
using Prism.Modularity;
using RestEase;
using System.Windows;

namespace DesktopClient;

public partial class App
{
    protected override Window CreateShell()
    {
        return Container.Resolve<MainWindow>();
    }

    protected override void RegisterTypes(IContainerRegistry containerRegistry)
    {
        var configuration = ConfigurationUtility
            .Configure(new ConfigurationBuilder())
            .Build();
        var mapperConfiguration = new MapperConfiguration(cfg =>
            cfg.AddProfile<AutoMapperProfile>());

        containerRegistry.Register<ICatalogClient>(services =>
            RestClient.For<ICatalogClient>(configuration["Urls:catalogClient"]));
        containerRegistry.Register<IMapper>(services => new Mapper(mapperConfiguration));

        containerRegistry.UseMvvmPrismDialogService();
    }

    protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
    {
        moduleCatalog.AddModule<CatalogModule.CatalogModule>();
        moduleCatalog.AddModule<TitleModule.TitleModule>();
    }
}
